//
//  ViewController.h
//  Splat
//

//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface ViewController : UIViewController

@end
