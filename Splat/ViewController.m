//
//  ViewController.m
//  Splat
//
//  Created by Beau Young on 14/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "ViewController.h"
#import "Title.h"
#import "MainMenu.h"
#import "Options.h"
#import "GameData.h"
#import "SelectTrashie.h"
#import "Game.h"
#import "Shop.h"

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [GameData sharedData];

    // Configure the view.
    SKView * skView = (SKView *)self.view;
    
    // Create and configure the scene.
    SKScene * scene = [Title sceneWithSize:skView.bounds.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:scene];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
    SKScene *game = [[MainMenu alloc] initWithSize:self.view.bounds.size];
    SKTransition *fade = [SKTransition fadeWithDuration:0.5];
    [(SKView *)self.view presentScene:game transition:fade];
}

@end
