//
//  TitleScene.m
//  TrashPack3
//
//  Created by Beau Young on 16/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "Title.h"
#import "EnterGame.h"
#import "GameData.h"

@implementation Title

- (id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {;
        [self createSceneContents];
    }
    return self;
}

- (void)createSceneContents {
    self.scaleMode = SKSceneScaleModeAspectFill;
    self.backgroundColor = [SKColor whiteColor];
    
    [[GameData sharedData] setupBackgroundMusicWithFileName:@"intro" ofType:@"aiff"];
    
    // Add Background image
    SKSpriteNode *backgroundImage = [SKSpriteNode spriteNodeWithImageNamed:@"titleBackground"];
    backgroundImage.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    backgroundImage.zPosition = 0;
    backgroundImage.size = CGSizeMake(self.frame.size.width, self.frame.size.height);
    [self addChild:backgroundImage];
    
    // Fade title, and move onto next scene after delay
    [self runAction:[self delay:1.6f] completion:^{
        [self nextScene];
    }];
}

- (SKAction *)delay:(float)seconds {
    SKAction *delay = [SKAction waitForDuration:seconds];
    return delay;
}

- (void)nextScene {
    SKScene *enterGameScene = [[EnterGame alloc] initWithSize:self.size];
    SKTransition *fade = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:enterGameScene transition:fade];
}

@end
