//
//  Shop.m
//  Splat
//
//  Created by Beau Young on 18/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "Shop.h"
#import "SKButtonNode.h"
#import "Options.h"
#import "GameData.h"

static NSString *seriesOneIdentifier = @"com.sharpagency.splat.seriesOneDeckPurchase";
static NSString *seriesTwoIdentifier = @"com.sharpagency.splat.seriesTwoDeckIAP";
static NSString *seriesThreeIdentifier = @"com.sharpagency.splat.seriesThreeDeckIAP";
static NSString *seriesFourIdentifier = @"com.sharpagency.splat.seriesFourDeckIAP";
static NSString *seriesFiveIdentifier = @"com.sharpagency.splat.seriesFiveDeckIAP";
static NSString *seriesSixIdentifier = @"com.sharpagency.splat.seriesSixDeckIAP";

@interface Shop()

@property (strong, nonatomic) SKTextureAtlas *textures;

@end

@implementation Shop

- (void)didMoveToView:(SKView *)view {
    self.scaleMode = SKSceneScaleModeAspectFill;
    self.backgroundColor = [SKColor whiteColor];
    self.textures = [SKTextureAtlas atlasNamed:@"shop"];
    [self createSceneContents];
}

- (void)createSceneContents {
    [self background];
    [self backButton];
    [self setupDecks];
    [self addSplatToPreviouslyChosenDeck];
}

- (void)background {
    SKSpriteNode *backgroundImage = [SKSpriteNode spriteNodeWithImageNamed:@"SHOP_bg.jpg"];
    backgroundImage.position = CGPointZero;
    backgroundImage.anchorPoint = CGPointZero;
    backgroundImage.zPosition = 1;
    [self addChild:backgroundImage];
}

- (void)backButton {
    SKButtonNode *backButton = [[SKButtonNode alloc] initWithTextureNormal:[self.textures textureNamed:@"backButton1"]
                                                                  selected:[self.textures textureNamed:@"backButton2"]];
    backButton.position = CGPointMake(CGRectGetMidX(self.frame), 70);
    backButton.zPosition = 3;
    [backButton setTouchUpInsideTarget:self action:@selector(backToOptionsScreen)];
    [self addChild:backButton];
}

#pragma mark - Purchased Deck Indicators
- (void)setupDecks {
    CGFloat xPos1 = 170; CGFloat xPos2 = 380; CGFloat xPos3 = 600;
    CGFloat yPos1 = 545; CGFloat yPos2 = 280;
    
    SKButtonNode *heroDeck = [[SKButtonNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(200, 250)];
    heroDeck.position = CGPointMake(500, 840);
    [heroDeck setTouchDownTarget:self action:@selector(cardTapped:)];
    heroDeck.name = @"heroDeck";
    heroDeck.zPosition = 3;
    [self addChild:heroDeck];
    
    SKButtonNode *deckOne = [[SKButtonNode alloc] initWithColor:[SKColor clearColor] size:CGSizeMake(160, 240)];
    deckOne.position = CGPointMake(xPos1, yPos1);
    deckOne.name = [NSString stringWithFormat:@"%@Deck", seriesOneIdentifier];
    deckOne.zPosition = 3;
    [deckOne setTouchDownTarget:self action:@selector(cardTapped:)];
    [self addChild:deckOne];
    
    SKButtonNode *deckTwo = [[SKButtonNode alloc] initWithColor:[SKColor clearColor] size:CGSizeMake(160, 240)];
    deckTwo.position = CGPointMake(xPos2, yPos1);
    deckTwo.name = [NSString stringWithFormat:@"%@Deck", seriesTwoIdentifier];
    [deckTwo setTouchDownTarget:self action:@selector(cardTapped:)];
    deckTwo.zPosition = 3;
    [self addChild:deckTwo];
    
    SKButtonNode *deckThree = [[SKButtonNode alloc] initWithColor:[SKColor clearColor] size:CGSizeMake(160, 240)];
    deckThree.position = CGPointMake(xPos3, yPos1);
    deckThree.name = [NSString stringWithFormat:@"%@Deck", seriesThreeIdentifier];
    [deckThree setTouchDownTarget:self action:@selector(cardTapped:)];
    deckThree.zPosition = 3;
    [self addChild:deckThree];
    
    SKButtonNode *deckFour = [[SKButtonNode alloc] initWithColor:[SKColor clearColor] size:CGSizeMake(160, 240)];
    deckFour.position = CGPointMake(xPos1, yPos2);
    deckFour.name = [NSString stringWithFormat:@"%@Deck", seriesFourIdentifier];
    [deckFour setTouchDownTarget:self action:@selector(cardTapped:)];
    deckFour.zPosition = 3;
    [self addChild:deckFour];
    
    SKButtonNode *deckFive = [[SKButtonNode alloc] initWithColor:[SKColor clearColor] size:CGSizeMake(160, 240)];
    deckFive.position = CGPointMake(xPos2, yPos2);
    deckFive.name = [NSString stringWithFormat:@"%@Deck", seriesFiveIdentifier];
    [deckFive setTouchDownTarget:self action:@selector(cardTapped:)];
    deckFive.zPosition = 3;
    [self addChild:deckFive];

    SKButtonNode *deckSix = [[SKButtonNode alloc] initWithColor:[SKColor clearColor] size:CGSizeMake(160, 240)];
    deckSix.position = CGPointMake(xPos3, yPos2);
    deckSix.name = [NSString stringWithFormat:@"%@Deck", seriesSixIdentifier];
    [deckSix setTouchDownTarget:self action:@selector(cardTapped:)];
    deckSix.zPosition = 3;
    [self addChild:deckSix];
}

- (void)cardTapped:(SKButtonNode *)sender {
    [GameData sharedData].selectedDeckName = sender.name;
    [self addSplatAtLocationOfNode:sender];
}

- (void)addSplatAtLocationOfNode:(SKNode *)node {
    [[self childNodeWithName:@"splat"] removeFromParent];
    [self runAction:[SKAction playSoundFileNamed:@"splat.caf" waitForCompletion:NO]];
    
    SKSpriteNode *splat = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"splat"]];
    splat.position = node.position;
    splat.zPosition = 10;
    splat.scale = 0.6;
    splat.name = @"splat";
    [self addChild:splat];
}

- (void)addSplatToPreviouslyChosenDeck {
    NSString *deckName = [GameData sharedData].selectedDeckName;
    NSLog(@"Previous DeckName: %@", deckName);
    [self addSplatAtLocationOfNode:[self childNodeWithName:deckName]];
}

- (void)backToOptionsScreen {
    SKScene *menuScreen = [[Options alloc] initWithSize:self.size];
    SKTransition *fade = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:menuScreen transition:fade];
}

@end