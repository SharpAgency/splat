//
//  SelectTrashie.m
//  Splat
//
//  Created by Beau Young on 17/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "SelectTrashie.h"
#import "SKButtonNode.h"
#import "MainMenu.h"
#import "GameData.h"
#import "Game.h"

@interface SelectTrashie()

@property (strong, nonatomic) SKTextureAtlas *textures;
@property (strong, nonatomic) NSMutableArray *trashies;

@end

@implementation SelectTrashie

- (void)didMoveToView:(SKView *)view {
    self.scaleMode = SKSceneScaleModeAspectFill;
    self.backgroundColor = [SKColor whiteColor];
    
    self.textures = [SKTextureAtlas atlasNamed:@"options"];
    
    [self createSceneContents];
}

- (void)createSceneContents {
    [self addChild:[self background]];
    [self setUpTrashies];
    [self addChild:[self backButton]];
    
    [self addChild:[self trashTalkerStoreBtn]];
    [self addChild:[self bookOneStoreBtn]];
    [self addChild:[self bookTwoStoreBtn]];
    [self addChild:[self rescueStoreBtn]];
}

- (SKSpriteNode *)background {
    SKSpriteNode *backgroundImage = [SKSpriteNode spriteNodeWithImageNamed:@"SPLAT_selection.jpg"];
    backgroundImage.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    backgroundImage.size = CGSizeMake(self.frame.size.width, self.frame.size.height);
    backgroundImage.name = @"background";
    return backgroundImage;
}

- (void)setUpTrashies {
    SKSpriteNode *mouldyMilk = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(120, 120)];
    mouldyMilk.position = CGPointMake(140, 520);
    mouldyMilk.name = @"mouldyMilk";
    [self addChild:mouldyMilk];
    
    SKSpriteNode *garbageGull = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(120, 120)];
    garbageGull.position = CGPointMake(290, 520);
    garbageGull.name = @"garbageGull";
    [self addChild:garbageGull];
    
    SKSpriteNode *rottenApple = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(120, 120)];
    rottenApple.position = CGPointMake(440, 520);
    rottenApple.name = @"rottenApple";
    [self addChild:rottenApple];
    
    SKSpriteNode *wastedBanana = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(120, 120)];
    wastedBanana.position = CGPointMake(610, 520);
    wastedBanana.name = @"wastedBanana";
    [self addChild:wastedBanana];

    SKSpriteNode *sourSnail = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(120, 120)];
    sourSnail.position = CGPointMake(140, 380);
    sourSnail.name = @"sourSnail";
    [self addChild:sourSnail];
    
    SKSpriteNode *trashRat = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(120, 120)];
    trashRat.position = CGPointMake(290, 380);
    trashRat.name = @"trashRat";
    [self addChild:trashRat];
    
    SKSpriteNode *scumGum = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(120, 120)];
    scumGum.position = CGPointMake(440, 380);
    scumGum.name = @"scumGum";
    [self addChild:scumGum];
    
    SKSpriteNode *trashAPillar = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(120, 120)];
    trashAPillar.position = CGPointMake(610, 380);
    trashAPillar.name = @"trashAPillar";
    [self addChild:trashAPillar];
    
    SKSpriteNode *trashola = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(120, 120)];
    trashola.position = CGPointMake(210, 250);
    trashola.name = @"trashola";
    [self addChild:trashola];
    
    SKSpriteNode *blowFly = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(120, 120)];
    blowFly.position = CGPointMake(360, 250);
    blowFly.name = @"blowFly";
    [self addChild:blowFly];
    
    SKSpriteNode *smellyFish = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(120, 120)];
    smellyFish.position = CGPointMake(510, 250);
    smellyFish.name = @"smellyFish";
    [self addChild:smellyFish];
}

- (SKButtonNode *)backButton {
    SKButtonNode *backButton = [[SKButtonNode alloc] initWithTextureNormal:[self.textures textureNamed:@"backButton1"] selected:[self.textures textureNamed:@"backButton2"]];
    backButton.position = CGPointMake(CGRectGetMidX(self.frame), 70);
    [backButton setTouchUpInsideTarget:self action:@selector(goToPreviousScene)];
    return backButton;
}

#define btnSize CGSizeMake(115, 115)
#define btnY 815
- (SKButtonNode *)trashTalkerStoreBtn {
    SKButtonNode *talkerBtn = [[SKButtonNode alloc] initWithColor:[UIColor clearColor] size:btnSize];
    talkerBtn.position = CGPointMake(178, btnY);
    [talkerBtn setTouchUpInsideTarget:self action:@selector(goToTalkerStore)];
    return talkerBtn;
}

- (SKButtonNode *)bookOneStoreBtn {
    SKButtonNode *bookBtn = [[SKButtonNode alloc] initWithColor:[UIColor clearColor] size:btnSize];
    bookBtn.position = CGPointMake(320, btnY);
    [bookBtn setTouchUpInsideTarget:self action:@selector(goToBookOneStore)];
    return bookBtn;
}

- (SKButtonNode *)bookTwoStoreBtn {
    SKButtonNode *bookBtn = [[SKButtonNode alloc] initWithColor:[UIColor clearColor] size:btnSize];
    bookBtn.position = CGPointMake(460, btnY);
    [bookBtn setTouchUpInsideTarget:self action:@selector(goToBookTwoStore)];
    return bookBtn;
}

- (SKButtonNode *)rescueStoreBtn {
    SKButtonNode *rescueBtn = [[SKButtonNode alloc] initWithColor:[UIColor clearColor] size:btnSize];
    rescueBtn.position = CGPointMake(590, btnY);
    [rescueBtn setTouchUpInsideTarget:self action:@selector(goToRescueStore)];
    return rescueBtn;
}

// Seperate methods must be used because the buttons are unable to send themselves.
- (void)goToTalkerStore {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/us/app/trash-talker/id778247287?mt=8"]];
}

- (void)goToBookOneStore {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/us/app/trash-pack-1/id586118957?mt=8"]];
}

- (void)goToBookTwoStore {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/us/app/trash-pack-2/id645415594?mt=8"]];
}

- (void)goToRescueStore {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/us/app/trash-talker/id778247287?mt=8"]];
}

- (void)goToPreviousScene {
    SKScene *mainMenu = [[MainMenu alloc] initWithSize:self.size];
    SKTransition *fade = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:mainMenu transition:fade];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:touchLocation];
    if (![node.name isEqualToString:@"background"]) {
        [[GameData sharedData] setSelectedTrashieName:node.name];
        [[GameData sharedData] setIsPlayWithTrashie:YES];
        [self addSplatAtLocation:node.position];
        [self runAction:[SKAction playSoundFileNamed:@"fart.caf" waitForCompletion:NO]];
        [self goToGameScene];
    }
}

- (void)addSplatAtLocation:(CGPoint)location {
    [[self childNodeWithName:@"splat"] removeFromParent];
    [self runAction:[SKAction playSoundFileNamed:@"splat.caf" waitForCompletion:NO]];
    
    SKSpriteNode *splat = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"Options_splat"]];
    splat.position = location;
    splat.name = @"splat";
    [self addChild:splat];
}

- (void)goToGameScene {
    [self runAction:[SKAction waitForDuration:.2] completion:^{
        SKScene *game = [[Game alloc] initWithSize:self.size];
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:game transition:fade];
    }];
}

@end
