//
//  MainMenuScene.m
//  Splat
//
//  Created by Beau Young on 14/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "MainMenu.h"
#import "SKButtonNode.h"
#import "SelectTrashie.h"
#import "Options.h"
#import "Game.h"
#import "GameData.h"

@interface MainMenu()

@property (strong, nonatomic) SKTextureAtlas *textures;
@property (strong, nonatomic) SKButtonNode *playWithTrashieButton;
@property (strong, nonatomic) SKButtonNode *playWithFriendButton;
@property (strong, nonatomic) SKButtonNode *optionsButton;

@end

@implementation MainMenu

- (void)didMoveToView:(SKView *)view {
    self.scaleMode = SKSceneScaleModeAspectFill;
    self.backgroundColor = [SKColor whiteColor];
    
    self.textures = [SKTextureAtlas atlasNamed:@"mainMenu"];
    [[GameData sharedData] setupBackgroundMusicWithFileName:@"bg" ofType:@"mp3"];
    
    [self createSceneContents];
}

- (void)createSceneContents {
    [self addChild:[self background]];
    [self addChild:[self playWithATrashieTouchZone]];
    [self addChild:[self playWithAFriendTouchZone]];
    [self addChild:[self optionsTouchZone]];
    [self addChild:[self instructionsButton]];
    [self addChild:[self boringStuffButton]];
    
    [self addCoinParticlesAtlocation:CGPointMake(500, 200)];
    [self addCoinParticlesAtlocation:CGPointMake(320, 910)];
    [self addCoinParticlesAtlocation:CGPointMake(165, 630)];
}

- (SKSpriteNode *)background {
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"Menu_BG.jpg"];
    background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    return background;
}

- (SKButtonNode *)playWithATrashieTouchZone {
    self.playWithTrashieButton = [[SKButtonNode alloc] initWithColor:[SKColor clearColor] size:CGSizeMake(210, 300)];
    self.playWithTrashieButton.position = CGPointMake(CGRectGetMidX(self.frame)-45, CGRectGetMidY(self.frame)+115);
    self.playWithTrashieButton.zRotation = M_PI/17.5;
    self.playWithTrashieButton.name = @"trashies";
    [self.playWithTrashieButton setTouchUpInsideTarget:self action:@selector(goToPlayWithATrashieScene)];
    return self.playWithTrashieButton;
}

- (SKButtonNode *)playWithAFriendTouchZone {
    self.playWithFriendButton = [[SKButtonNode alloc] initWithColor:[SKColor clearColor] size:CGSizeMake(210, 300)];
    self.playWithFriendButton.position = CGPointMake(CGRectGetMidX(self.frame)+200, CGRectGetMidY(self.frame)-50);
    self.playWithFriendButton.zRotation = M_PI/1.08;
    self.playWithFriendButton.name = @"friends";
    [self.playWithFriendButton setTouchUpInsideTarget:self action:@selector(goToGameScene)];
    return self.playWithFriendButton;
}

- (SKButtonNode *)optionsTouchZone {
    self.optionsButton = [[SKButtonNode alloc] initWithColor:[SKColor clearColor] size:CGSizeMake(210, 300)];
    self.optionsButton.position = CGPointMake(CGRectGetMidX(self.frame)-25, 265);
    self.optionsButton.zRotation = M_PI/1.1;
    self.optionsButton.name = @"options";
    [self.optionsButton setTouchUpInsideTarget:self action:@selector(goToOptionsScene)];
    return self.optionsButton;
}

- (SKButtonNode *)instructionsButton {
    SKButtonNode *button = [[SKButtonNode alloc] initWithTextureNormal:[self.textures textureNamed:@"instructionBtn"] selected:[self.textures textureNamed:@"instructionBtnHigh"]];
    button.position = CGPointMake(CGRectGetMaxX(self.frame)-button.size.width/2, button.size.height/2);
    [button setTouchUpInsideTarget:self action:@selector(showInstructionsPage:)];
    button.scale = 0.6f;
    button.name = @"Instructions";
    return button;
}

- (SKButtonNode *)boringStuffButton {
    SKButtonNode *button = [[SKButtonNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(160, 90)];
    button.position = CGPointMake(105, 100);
    [button setTouchUpInsideTarget:self action:@selector(showBoringStuffPage)];
    return button;
}

- (void)showBoringStuffPage {
    SKButtonNode *boringPage = [[SKButtonNode alloc] initWithImageNamed:@"boringStuff.jpg"];
    boringPage.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    [boringPage setTouchUpInsideTarget:self action:@selector(removeSenderFromParent:)];
    boringPage.name = @"fullScreen";
    boringPage.userInteractionEnabled = NO;
    [self addChild:boringPage];
}

- (void)showInstructionsPage:(SKButtonNode *)sender {
    SKButtonNode *instructionsPage = [[SKButtonNode alloc] initWithImageNamed:@"HOWTO_SCREEN.jpg"];
    instructionsPage.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    instructionsPage.name = @"fullScreen";
    instructionsPage.userInteractionEnabled = NO;
    [instructionsPage setTouchUpInsideTarget:self action:@selector(removeSenderFromParent:)];
    [self addChild:instructionsPage];
}

- (void)removeSenderFromParent:(SKButtonNode *)sender {
    [sender removeFromParent];
}

- (void)addCoinParticlesAtlocation:(CGPoint)location {
    SKEmitterNode *sparkle = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"sparkle" ofType:@"sks"]] ;
    
    sparkle.position = location;
    sparkle.particleBirthRate = .5 + arc4random() % 1;
    
    [self addChild:sparkle];
}

- (void)goToPlayWithATrashieScene {
    [self addSplatAtLocation:self.playWithTrashieButton.position];

    [self runAction:[SKAction waitForDuration:.2] completion:^{
        SKScene *selectTrashie = [[SelectTrashie alloc] initWithSize:self.size];
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:selectTrashie transition:fade];
    }];
}

- (void)goToGameScene {
    [self addSplatAtLocation:self.playWithFriendButton.position];
    [GameData sharedData].isPlayWithTrashie = NO;
    
    [self runAction:[SKAction waitForDuration:0.2] completion:^{
        SKScene *game = [[Game alloc] initWithSize:self.size];
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:game transition:fade];
    }];
}

- (void)goToOptionsScene {
    [self addSplatAtLocation:self.optionsButton.position];
    
    [self runAction:[SKAction waitForDuration:.2] completion:^{
        SKScene *options = [[Options alloc] initWithSize:self.size];
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:options transition:fade];
    }];
}

- (void)addSplatAtLocation:(CGPoint)location {
    [self runAction:[SKAction playSoundFileNamed:@"splat.caf" waitForCompletion:NO]];
    SKSpriteNode *splat = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"splat"]];
    splat.position = location;
    [self addChild:splat];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self enumerateChildNodesWithName:@"fullScreen" usingBlock:^(SKNode *node, BOOL *stop) {
        [node removeFromParent];
    }];
}

@end
