//
//  Options.m
//  Splat
//
//  Created by Beau Young on 17/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "Options.h"
#import "MainMenu.h"
#import "Shop.h"
#import "GameData.h"
#import "SKButtonNode.h"

@interface Options()
@property (strong, nonatomic) UISlider *musicVolumeSlider;
@property (strong, nonatomic) UISlider *soundEffectsSlider;
@property (strong, nonatomic) UISlider *difficultySlider;

@property (strong, nonatomic) SKTextureAtlas *textures;

@property (strong, nonatomic) SKButtonNode *dingyDinerButton;
@property (strong, nonatomic) SKButtonNode *crudBeachButton;
@property (strong, nonatomic) SKButtonNode *sewerDoorButton;
@property (strong, nonatomic) SKButtonNode *trashAlley;

@end
@implementation Options

- (void)didMoveToView:(SKView *)view {
    self.scaleMode = SKSceneScaleModeAspectFill;
    self.backgroundColor = [SKColor whiteColor];
    
    self.textures = [SKTextureAtlas atlasNamed:@"options"];
    
    [self createSceneContents];
}

- (void)createSceneContents {
    [self addChild:[self background]];
    [self addChild:[self backButton]];
    [self addChild:[self shopButton]];
    
    [self setUpSliders];
    [self setUpBackgroundSelector];
}

- (SKSpriteNode *)background {
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"Options_BG.jpg"];
    background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    return background;
}

- (SKButtonNode *)backButton {
    SKButtonNode *backButton = [[SKButtonNode alloc] initWithTextureNormal:[self.textures textureNamed:@"backButton1"]
                                                              selected:[self.textures textureNamed:@"backButton2"]];
    backButton.position = CGPointMake(CGRectGetMidX(self.frame), 70);
    backButton.zPosition = 3;
    [backButton setTouchUpInsideTarget:self action:@selector(goToMainMenu)];
    return backButton;
}

- (SKButtonNode *)shopButton {
    SKButtonNode *shopButton = [[SKButtonNode alloc] initWithTextureNormal:[self.textures textureNamed:@"myDecksNormal"]
                                                              selected:[self.textures textureNamed:@"myDecksSelected"]];
    shopButton.position = CGPointMake(CGRectGetMidX(self.frame)-3, 223);
    [shopButton setTouchUpInsideTarget:self action:@selector(goToShopMenu)];
    return shopButton;
}

- (void)setUpSliders {
    self.musicVolumeSlider = [[UISlider alloc] initWithFrame:CGRectMake(327, 160, 375, 30)];
    self.musicVolumeSlider.minimumValue = 0.0;
    self.musicVolumeSlider.maximumValue = 1.0;
    self.musicVolumeSlider.minimumTrackTintColor = [UIColor greenColor];
    self.musicVolumeSlider.continuous = YES;
    self.musicVolumeSlider.value = [GameData sharedData].musicLevel;
    [self.musicVolumeSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.musicVolumeSlider];
    
    self.soundEffectsSlider = [[UISlider alloc] initWithFrame:CGRectMake(327, 210, 375, 30)];
    self.soundEffectsSlider.minimumValue = 0.0;
    self.soundEffectsSlider.maximumValue = 1.0;
    self.soundEffectsSlider.minimumTrackTintColor = [UIColor greenColor];
    self.soundEffectsSlider.continuous = YES;
    self.soundEffectsSlider.value = [GameData sharedData].soundFXLevel;
    [self.soundEffectsSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.soundEffectsSlider];
    
    self.difficultySlider = [[UISlider alloc] initWithFrame:CGRectMake(327, 260, 375, 30)];
    self.difficultySlider.minimumValue = 1.0;
    self.difficultySlider.maximumValue = 5.0;
    self.difficultySlider.minimumTrackTintColor = [UIColor greenColor];
    self.difficultySlider.continuous = YES;
    self.difficultySlider.value = (self.difficultySlider.maximumValue - [GameData sharedData].difficultyLevel);
    [self.difficultySlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.difficultySlider];
}

- (void)setUpBackgroundSelector {
    self.dingyDinerButton = [[SKButtonNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(160, 210)];
    self.dingyDinerButton.position = CGPointMake(140, 480);
    [self.dingyDinerButton setTouchUpInsideTarget:self action:@selector(selectDingyDiner)];
    [self addChild:self.dingyDinerButton];
    
    self.crudBeachButton = [[SKButtonNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(160, 210)];
    self.crudBeachButton.position = CGPointMake(302, 480);
    [self.crudBeachButton setTouchUpInsideTarget:self action:@selector(selectCrudBeach)];
    [self addChild:self.crudBeachButton];
    
    
    self.sewerDoorButton = [[SKButtonNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(160, 210)];
    self.sewerDoorButton.position = CGPointMake(462, 480);
    [self.sewerDoorButton setTouchUpInsideTarget:self action:@selector(selectSewerDoor)];
    [self addChild:self.sewerDoorButton];
    
    
    self.trashAlley = [[SKButtonNode alloc] initWithColor:[UIColor clearColor] size:CGSizeMake(160, 210)];
    self.trashAlley.position = CGPointMake(622, 480);
    [self.trashAlley setTouchUpInsideTarget:self action:@selector(selectTrashAlley)];
    [self addChild:self.trashAlley];
    
    [self initSplat];
}

- (void)sliderValueChanged:(UISlider *)sender {
    if (sender == self.musicVolumeSlider) {
        [GameData sharedData].musicLevel = sender.value;
    }
    else if (sender == self.soundEffectsSlider) {
        [GameData sharedData].soundFXLevel = sender.value;
    }
    else if (sender == self.difficultySlider) {
        [GameData sharedData].difficultyLevel = (sender.maximumValue - sender.value);
    }
}

- (void)selectDingyDiner {
    [self runAction:[SKAction playSoundFileNamed:@"splat.caf" waitForCompletion:NO]];
    [self addSplatAtLocation:self.dingyDinerButton.position];
    [[GameData sharedData] setSelectedBackgroundIndex:0];
}
- (void)selectCrudBeach {
    [self runAction:[SKAction playSoundFileNamed:@"splat.caf" waitForCompletion:NO]];
    [self addSplatAtLocation:self.crudBeachButton.position];
    
    [[GameData sharedData] setSelectedBackgroundIndex:1];
}
- (void)selectSewerDoor {
    [self runAction:[SKAction playSoundFileNamed:@"splat.caf" waitForCompletion:NO]];
    [self addSplatAtLocation:self.sewerDoorButton.position];
    [[GameData sharedData] setSelectedBackgroundIndex:2];
}
- (void)selectTrashAlley {
    [self runAction:[SKAction playSoundFileNamed:@"splat.caf" waitForCompletion:NO]];
    [self addSplatAtLocation:self.trashAlley.position];
    [[GameData sharedData] setSelectedBackgroundIndex:3];
}

- (void)initSplat {
    if ([GameData sharedData].selectedBackgroundIndex == 0) {
        [self addSplatAtLocation:self.dingyDinerButton.position];
    }
    else if ([GameData sharedData].selectedBackgroundIndex == 1) {
        [self addSplatAtLocation:self.crudBeachButton.position];
    }
    else if ([GameData sharedData].selectedBackgroundIndex == 2) {
        [self addSplatAtLocation:self.sewerDoorButton.position];
    }
    else if ([GameData sharedData].selectedBackgroundIndex == 3) {
        [self addSplatAtLocation:self.trashAlley.position];
    }
}

- (void)addSplatAtLocation:(CGPoint)location {
    [[self childNodeWithName:@"splat"] removeFromParent];
    
    SKSpriteNode *splat = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"Options_splat"]];
    splat.position = location;
    splat.name = @"splat";
    [self addChild:splat];
}

- (void)goToShopMenu {
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    SKScene *shopMenu = [[Shop alloc] initWithSize:self.size];
    SKTransition *fade = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:shopMenu transition:fade];
}

- (void)goToMainMenu {
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    SKScene *mainMenu = [[MainMenu alloc] initWithSize:self.size];
    SKTransition *fade = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:mainMenu transition:fade];
}

@end
