//
//  Game.m
//  Splat
//
//  Created by Beau Young on 17/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "Game.h"
#import "GameData.h"
#import "Deck.h"
#import "Card.h"
#import "CharacterArm.h"
#import "SKButtonNode.h"
#import "SelectTrashie.h"
#import "MainMenu.h"
#import "CardNode.h"
#import "DeckNode.h"

@interface Game()

@property (nonatomic) int playerOneScore;
@property (nonatomic) int playerTwoScore;

@property (nonatomic) BOOL isPlayerOneTurn;
@property (nonatomic) BOOL isPlayerTwoTurn;
@property (nonatomic) BOOL cardsMatch;

@property (strong, nonatomic) CharacterArm *trashieArm;
@property (strong, nonatomic) SKTextureAtlas *textures;

@property (strong, nonatomic) SKButtonNode *playerOneSplatButton;
@property (strong, nonatomic) SKButtonNode *playerTwoSplatButton;

@property (strong, nonatomic) SKSpriteNode *playerOneScoreTrackerNode;
@property (strong, nonatomic) SKSpriteNode *playerTwoScoreTrackerNode;
@property (strong, nonatomic) DeckNode *playerOneDeck;
@property (strong, nonatomic) DeckNode *playerTwoDeck;

@property (strong, nonatomic) Card *playerOneCard;
@property (strong, nonatomic) Card *playerTwoCard;

@property (strong, nonatomic) Deck *deck;

@property (strong, nonatomic) NSMutableArray *cardStack;

@property (strong, nonatomic) NSArray *playerScoreTextures;

@end

@implementation Game

- (void)didMoveToView:(SKView *)view {
    self.scaleMode = SKSceneScaleModeAspectFill;
    self.backgroundColor = [SKColor whiteColor];
    
    self.isPlayerOneTurn = YES;
    
    self.textures = [SKTextureAtlas atlasNamed:@"game"];
    
    _playerOneScore = 0;
    _playerTwoScore = 0;
    
    [self createSceneContents];
}

- (void)createSceneContents {
    [self addChild:[self backgroundImage]];
    [self addChild:[self backButton]];
    [self addChild:[self splatButtons]];
    [self setupScoreTrackingNodes];
    [self createDecks];
    
    self.cardStack = [[NSMutableArray alloc] initWithCapacity:4];
    
    _cardsMatch = NO;
    
    if ([GameData sharedData].isPlayWithTrashie) {
        [self addChild:[self characterArm]];
    }
}

#pragma mark - Setup Methods
- (SKSpriteNode *)backgroundImage {
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:[[GameData sharedData] getBackgroundImageName]];
    background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    return background;
}

- (SKButtonNode *)backButton {
    SKButtonNode *backBtn = [[SKButtonNode alloc] initWithTextureNormal:[self.textures textureNamed:@"backButton1"] selected:[self.textures textureNamed:@"backButton2"]];
    backBtn.scale = .7;
    backBtn.position = CGPointMake(CGRectGetMidX(self.frame), backBtn.size.height);
    [backBtn setTouchUpInsideTarget:self action:@selector(goBackToMenu)];
    return backBtn;
}

- (SKNode *)splatButtons {
    SKNode *splatButtons = [[SKNode alloc] init];
    
    self.playerOneSplatButton = [[SKButtonNode alloc] initWithTextureNormal:[self.textures textureNamed:@"SPLATbutton_green"]
                                                                   selected:[self.textures textureNamed:@"SPLATbutton_greenBright"]
                                                                   disabled:[self.textures textureNamed:@"SPLATbutton_red"]];
    
    [self.playerOneSplatButton setTouchUpInsideTarget:self action:@selector(playerOneSplatButtonTapped)];
    self.playerOneSplatButton.position = CGPointMake(CGRectGetMidX(self.frame)+90, 230);
    self.playerOneSplatButton.scale = 0.9;
    [splatButtons addChild:self.playerOneSplatButton];
    
    
    self.playerTwoSplatButton = [[SKButtonNode alloc] initWithTextureNormal:[self.textures textureNamed:@"SPLATbutton_green"]
                                                                   selected:[self.textures textureNamed:@"SPLATbutton_greenBright"]
                                                                   disabled:[self.textures textureNamed:@"SPLATbutton_red"]];
    
    [self.playerTwoSplatButton setTouchUpInsideTarget:self action:@selector(playerTwoSplatButtonTapped)];
    self.playerTwoSplatButton.position = CGPointMake(CGRectGetMidX(self.frame)-90, 800);
    self.playerTwoSplatButton.zRotation = M_PI;
    self.playerTwoSplatButton.scale = 0.9;
    [splatButtons addChild:self.playerTwoSplatButton];
    
    return splatButtons;
}

- (CharacterArm *)characterArm {
    SKTextureAtlas *armAtlas = [SKTextureAtlas atlasNamed:@"tp_hands"];
    
    self.trashieArm = [[CharacterArm alloc] initWithTexture:[armAtlas textureNamed:[GameData sharedData].selectedTrashieName]];
    self.trashieArm.position = CGPointMake(700, 1000);
    self.trashieArm.zPosition = 30;
    
    return self.trashieArm;
}

- (void)createDecks {
    self.deck = [[Deck alloc] init];
    [_deck shuffle];
    
    self.playerOneDeck = [DeckNode spriteNodeWithImageNamed:@"deckStack"];
    self.playerOneDeck.position = CGPointMake(150, 180);
    self.playerOneDeck.name = @"playerOneDeck";
    [_playerOneDeck startPulse];
    [self addChild:self.playerOneDeck];
    
    self.playerTwoDeck = [DeckNode spriteNodeWithImageNamed:@"deckStack"];
    self.playerTwoDeck.position = CGPointMake(630, 840);
    self.playerTwoDeck.name = @"playerTwoDeck";
    self.playerTwoDeck.zRotation = M_PI;
    [self addChild:self.playerTwoDeck];
}

- (void)setupScoreTrackingNodes {
    self.playerScoreTextures = @[[self.textures textureNamed:@"scoreZero"],
                                 [self.textures textureNamed:@"scoreOne"],
                                 [self.textures textureNamed:@"scoreTwo"],
                                 [self.textures textureNamed:@"scoreThree"],
                                 [self.textures textureNamed:@"scoreFour"],
                                 [self.textures textureNamed:@"scoreFive"]];
    
    self.playerOneScoreTrackerNode = [SKSpriteNode spriteNodeWithTexture:[_playerScoreTextures objectAtIndex:_playerOneScore]];
    self.playerOneScoreTrackerNode.position = CGPointMake(self.frame.size.width-(self.playerOneScoreTrackerNode.size.width/1.4), self.playerOneScoreTrackerNode.size.height/1.8);
    self.playerOneScoreTrackerNode.zPosition = 20;
    [self addChild:self.playerOneScoreTrackerNode];
    
    self.playerTwoScoreTrackerNode = [SKSpriteNode spriteNodeWithTexture:[_playerScoreTextures objectAtIndex:_playerTwoScore]];
    self.playerTwoScoreTrackerNode.position = CGPointMake(self.playerTwoScoreTrackerNode.size.width/1.4, 840);
    self.playerTwoScoreTrackerNode.zRotation = M_PI;
    self.playerTwoScoreTrackerNode.zPosition = 20;
    [self addChild:self.playerTwoScoreTrackerNode];
}

#pragma mark - Setters
- (void)setIsPlayerOneTurn:(BOOL)isPlayerOneTurn {
    [_playerTwoDeck stopPulse];
    [_playerOneDeck startPulse];
    
    _isPlayerOneTurn = isPlayerOneTurn;
    _isPlayerTwoTurn = !isPlayerOneTurn;
}

- (void)setIsPlayerTwoTurn:(BOOL)isPlayerTwoTurn {
    [_playerOneDeck stopPulse];
    [_playerTwoDeck startPulse];
    
    _isPlayerTwoTurn = isPlayerTwoTurn;
    _isPlayerOneTurn = !isPlayerTwoTurn;
    if ([GameData sharedData].isPlayWithTrashie && ![GameData sharedData].isSpecialCard) {
        if (!_cardsMatch) {
            [self trashieDealCard];
        }
    }
}

#pragma mark - Trashie AI
- (void)trashieDealCard {
    if (_isPlayerTwoTurn) {
        SKAction *delay = [SKAction waitForDuration:1];
        [self.trashieArm runAction:delay completion:^ {
            [self.trashieArm flipCardAction];
            [self dealCard];
            self.isPlayerOneTurn = YES;
        }];
    }
}

- (void)trashieHitSplat {
        SKAction *delay = [SKAction waitForDuration:[[GameData sharedData] getDifficultyDelay]];
        [self.trashieArm runAction:delay completion:^{
            if (_cardsMatch) {
                [self.trashieArm pressSplatAction];
                [self playerTwoSplatButtonTapped];
                if (_isPlayerTwoTurn) {
                    [self trashieDealCard];
                }
            }
        }];
}

#pragma mark - Point System
- (void)setPlayerOneScore:(int)playerOneScore { // Textures should be stored in an array and accessed using the score as index.
    _playerOneScore = playerOneScore;
    
    self.playerOneScoreTrackerNode.texture = [_playerScoreTextures objectAtIndex:playerOneScore];
    [self showParticleForScore:_playerOneScore ofPlayer:@"playerOne"];
    
    if (playerOneScore == 5) {
        [self.trashieArm runAction:[SKAction moveByX:0.0 y:600 duration:0.5]completion:^{
            [self.trashieArm removeFromParent];
        }];
        [self playWinSequenceForPlayer:@"playerOne"];
    }
}

- (void)setPlayerTwoScore:(int)playerTwoScore {
    _playerTwoScore = playerTwoScore;
    
    self.playerTwoScoreTrackerNode.texture = [_playerScoreTextures objectAtIndex:playerTwoScore];
    [self showParticleForScore:_playerTwoScore ofPlayer:@"playerTwo"];

    if (playerTwoScore == 5) {
        [self playWinSequenceForPlayer:@"playerTwo"];
    }
}

- (void)showParticleForScore:(int)score ofPlayer:(NSString *)player {
    SKEmitterNode *sparkle = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"pointSpark" ofType:@"sks"]] ;
    
    if ([player isEqualToString:@"playerOne"]) {
        sparkle.yAcceleration = 1000;
        if (score == 1) sparkle.position = CGPointMake(CGRectGetMaxX(self.frame)-45, 300);
        if (score == 2) sparkle.position = CGPointMake(CGRectGetMaxX(self.frame)-45, 260);
        if (score == 3) sparkle.position = CGPointMake(CGRectGetMaxX(self.frame)-45, 200);
        if (score == 4) sparkle.position = CGPointMake(CGRectGetMaxX(self.frame)-45, 130);
        if (score == 5) sparkle.position = CGPointMake(CGRectGetMaxX(self.frame)-45, 50);
    }
    else if ([player isEqualToString:@"playerTwo"]) {
        sparkle.yAcceleration = -1000;
        if (score == 1) sparkle.position = CGPointMake(45, CGRectGetMaxY(self.frame)-300);
        if (score == 2) sparkle.position = CGPointMake(45, CGRectGetMaxY(self.frame)-260);
        if (score == 3) sparkle.position = CGPointMake(45, CGRectGetMaxY(self.frame)-200);
        if (score == 4) sparkle.position = CGPointMake(45, CGRectGetMaxY(self.frame)-130);
        if (score == 5) sparkle.position = CGPointMake(45, CGRectGetMaxY(self.frame)-50);
    }
    [self runAction:[SKAction playSoundFileNamed:@"sparkle.caf" waitForCompletion:NO]];
    [self addChild:sparkle];
}

#pragma mark - Touch Recognizers
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint positionInScene = [touch locationInNode:self];
    
    SKNode *node = (SKNode *)[self nodeAtPoint:positionInScene];
    if ([node.name isEqualToString:@"playerOneDeck"] && _isPlayerOneTurn) {
        [self dealCard];
        self.isPlayerTwoTurn = YES;
    }
    else if ([node.name isEqualToString:@"playerTwoDeck"] && _isPlayerTwoTurn) {
        [self dealCard];
        self.isPlayerOneTurn = YES;
    }
}

#pragma mark - Gameplay
- (void)checkIfCardsMatch {
    if (_playerOneCard.suit == _playerTwoCard.suit) {
        _cardsMatch = YES;
        if ([GameData sharedData].isPlayWithTrashie) {
            [self trashieHitSplat];
        }
    }
    else if (_playerOneCard.value == _playerTwoCard.value) {
        _cardsMatch = YES;
        if ([GameData sharedData].isPlayWithTrashie) {
            [self trashieHitSplat];
        }
    }
    else _cardsMatch = NO;
}

- (void)playerOneSplatButtonTapped {
    if (_playerOneCard && _cardsMatch) {
        [self showSplat];
        self.playerOneScore++;
        _cardsMatch = NO;
        if (_isPlayerTwoTurn && [GameData sharedData].isPlayWithTrashie) {
            [self trashieDealCard];
        }
    }
    else {
        self.playerOneSplatButton.isEnabled = NO;
        [self runAction:[SKAction playSoundFileNamed:@"wrong.caf" waitForCompletion:NO]];
        [self.playerOneSplatButton runAction:[SKAction waitForDuration:.1] completion:^{
            self.playerOneSplatButton.isEnabled = YES;
        }];
    }
}

- (void)playerTwoSplatButtonTapped {
    if (_playerTwoCard && _cardsMatch) {
        [self showSplat];
        self.playerTwoScore++;
        _cardsMatch = NO;
        if ([GameData sharedData].isPlayWithTrashie && _playerTwoScore < 5) {
            [self runAction:[SKAction playSoundFileNamed:[NSString stringWithFormat:@"%@_woohoo.caf", [GameData sharedData].selectedTrashieName] waitForCompletion:NO]];
        }
    }
    else {
        self.playerTwoSplatButton.isEnabled = NO;
        [self runAction:[SKAction playSoundFileNamed:@"wrong.caf" waitForCompletion:NO]];
        [self.playerTwoSplatButton runAction:[SKAction waitForDuration:.1] completion:^ {
            self.playerTwoSplatButton.isEnabled = YES;
        }];
    }
}

- (void)dealCard {
    if (_isPlayerOneTurn) {
        self.playerOneCard = [_deck draw];
        // run animations and create new spritecard to throw in.
        CardNode *playerOneCard = [[CardNode alloc] initWithImageNamed:@"cardback_moose"];
        [playerOneCard loadTextureForCard:_playerOneCard];
        playerOneCard.position = _playerOneDeck.position;
        [self runAction:[SKAction playSoundFileNamed:@"cardFlip.caf" waitForCompletion:NO]];
        [self addChild:playerOneCard];
        [playerOneCard runAction:[self throwInCard]];
        [_cardStack insertObject:playerOneCard atIndex:0];
        if ([GameData sharedData].isSpecialCard) {
            [self playSpecialWinSequenceForPlayer:@"playerOne"];
        }
    }
    
    if (_isPlayerTwoTurn) {
        self.playerTwoCard = [_deck draw];
        CardNode *playerTwoCard = [[CardNode alloc] initWithImageNamed:@"cardback_moose"];
        [playerTwoCard loadTextureForCard:_playerTwoCard];
        playerTwoCard.position = _playerTwoDeck.position;
        [self runAction:[SKAction playSoundFileNamed:@"cardFlip.caf" waitForCompletion:NO]];
        [self addChild:playerTwoCard];
        [playerTwoCard runAction:[self throwInCard]];
        [_cardStack insertObject:playerTwoCard atIndex:0];
        if ([GameData sharedData].isSpecialCard) {
            [self playSpecialWinSequenceForPlayer:@"playerTwo"];
        }
    }
    [self checkIfCardsMatch];
    if (_cardStack.count >= 10) {
        [[_cardStack objectAtIndex:9] runAction:[self fadeAndRemoveFromParent]];
    }
}

- (SKAction *)throwInCard {
    SKAction *moveIn = [SKAction moveTo:CGPointMake(CGRectGetMidX(self.frame)+arc4random_uniform(180) - 90, CGRectGetMidY(self.frame)+arc4random_uniform(70) - 35) duration:0.3];
    SKAction *rotate = [SKAction rotateByAngle:arc4random_uniform(5) + 2 duration:0.3];
    SKAction *group = [SKAction group:@[rotate, moveIn]];
    return group;
}

- (SKAction *)fadeAndRemoveFromParent {
    SKAction *fadeAway = [SKAction fadeOutWithDuration:0.25];
    SKAction *remove = [SKAction removeFromParent];
    SKAction *moveSequence = [SKAction sequence:@[fadeAway, remove]];
    return moveSequence;
}
- (void)showSplat {
    SKSpriteNode *splat = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"SPLAT"]];
    splat.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    splat.zPosition = 30;
    [self addChild:splat];
    [self runAction:[SKAction playSoundFileNamed:@"splat.caf" waitForCompletion:NO]];
    [splat runAction:[SKAction waitForDuration:0.5] completion:^{
        [splat runAction:[self fadeAndRemoveFromParent]];
    }];
}

#pragma mark - Win Sequence
- (void)playWinSequenceForPlayer:(NSString *)player {
    self.userInteractionEnabled = NO;
    [self runAction:[SKAction playSoundFileNamed:@"win.aif" waitForCompletion:NO]];
    [[GameData sharedData].bgMusic stop];
    
    SKSpriteNode *youWon = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"YOUWON"]];
    youWon.zPosition = 50;
    youWon.scale = 0.5;
    if ([player isEqualToString:@"playerOne"]) {
        youWon.position = CGPointMake(CGRectGetMidX(self.frame), youWon.size.height);
        [self addChild:youWon];
    }
    else {
        youWon.zRotation = M_PI;
        youWon.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame)-(youWon.size.height));
        [self addChild:youWon];

        if ([GameData sharedData].isPlayWithTrashie) {
            [self runAction:[SKAction playSoundFileNamed:[NSString stringWithFormat:@"%@_haha.caf", [GameData sharedData].selectedTrashieName] waitForCompletion:NO]];
        }
    }
    SKAction *scaleUp = [SKAction scaleTo:1.2 duration:0.3];
    SKAction *scaleDown = [SKAction scaleTo:1.0 duration:0.3];
    [youWon runAction:[SKAction repeatActionForever:[SKAction sequence:@[scaleUp, scaleDown]]]];
    
    [self addMagicParticles];
    [self restartGame];
}

- (void)playSpecialWinSequenceForPlayer:(NSString *)player {
    self.userInteractionEnabled = NO;
    [[GameData sharedData].bgMusic stop];
    
    if ([GameData sharedData].isPlayWithTrashie) {
        [self.trashieArm runAction:[SKAction moveByX:0.0 y:600 duration:0.5]completion:^{
            [self.trashieArm removeFromParent];
        }];
    }

    [self runAction:[SKAction playSoundFileNamed:@"win.aif" waitForCompletion:NO]];
    SKSpriteNode *card = [_cardStack objectAtIndex:0];
    card.zPosition = 50;

    SKAction *scaleUp = [SKAction scaleTo:2.0 duration:0.3];
    SKAction *scaleDown = [SKAction scaleTo:1.9 duration:0.3];
    SKAction *upAndPulse = [SKAction sequence:@[scaleUp, scaleDown]];
    
    SKSpriteNode *youWon = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"YOUWON"]];
    youWon.zPosition = 50;
    youWon.scale = 0.5;
    
    if ([player isEqualToString:@"playerOne"]) {
        self.playerOneScoreTrackerNode.texture = [self.playerScoreTextures objectAtIndex:5];
        youWon.position = CGPointMake(CGRectGetMidX(self.frame), youWon.size.height);
        [card runAction:[SKAction rotateToAngle:0 duration:0.3 shortestUnitArc:YES]];
        [card runAction:[SKAction repeatActionForever:upAndPulse]];
    }
    else {
        self.playerTwoScoreTrackerNode.texture = [self.playerScoreTextures objectAtIndex:5];
        youWon.zRotation = M_PI;
        youWon.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame)-(youWon.size.height));
        [card runAction:[SKAction rotateToAngle:M_PI duration:0.3 shortestUnitArc:YES]];
        [card runAction:[SKAction repeatActionForever:upAndPulse]];
    }
    
    SKEmitterNode *cardParticle = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"stars" ofType:@"sks"]] ;
    cardParticle.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    cardParticle.zPosition = 20;
    [self addChild:cardParticle];
    
    [self restartGame];
}

- (void)addMagicParticles {
    SKEmitterNode *cardParticle = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:@"winParticles" ofType:@"sks"]] ;
    cardParticle.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame)+300);
    cardParticle.zPosition = 20;
    [self addChild:cardParticle];
}

#pragma mark - Navigation
- (void)goBackToMenu {
    if ([GameData sharedData].isPlayWithTrashie) {
        SKScene *selectTrashie = [[SelectTrashie alloc] initWithSize:self.size];
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:selectTrashie transition:fade];
    } else {
        SKScene *menu = [[MainMenu alloc] initWithSize:self.size];
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:menu transition:fade];
    }
}

- (void)restartGame {
    [[GameData sharedData] setIsSpecialCard:NO];
    [self runAction:[SKAction waitForDuration:4] completion:^{
        SKScene *game = [[Game alloc] initWithSize:self.size];
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:game transition:fade];
    }];
}

@end
