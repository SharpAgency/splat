//
//  EnterGame.m
//  TrashPack3
//
//  Created by Beau Young on 16/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "EnterGame.h"
#import "MainMenu.h"

#define FOLLOW_PATH_SPEED 1.5
#define BEZIER_HEIGHT_ADDITION 180
#define BEZIER_X_SUBTRACTION 140

@interface EnterGame()

@property (strong, nonatomic) SKTextureAtlas *textures;
@property (strong, nonatomic) NSMutableArray *flyingTrash;
@property BOOL trashIsMovingLeft;
@property BOOL FlyingTrashCreated;

@end

@implementation EnterGame

- (void)didMoveToView:(SKView *)view {
    self.scaleMode = SKSceneScaleModeAspectFill;
    self.backgroundColor = [SKColor whiteColor];
    
    self.textures = [SKTextureAtlas atlasNamed:@"enterScreen"];
    self.flyingTrash = [[NSMutableArray alloc] init];
    
    [self createSceneContents];
}

- (void)createSceneContents {
    SKAction *moveCloudsUp = [SKAction moveBy:CGVectorMake(0, 550) duration:2];
    SKAction *moveFenceUp = [SKAction moveBy:CGVectorMake(0, 220) duration:.7];
    SKAction *delay = [SKAction waitForDuration:1.3];
    SKAction *moveSequence = [SKAction sequence:@[delay, moveFenceUp]];
    
    // sky
    SKSpriteNode *sky = [SKSpriteNode spriteNodeWithImageNamed:@"sky"];
    sky.position = CGPointMake(CGRectGetMidX(self.frame), (CGRectGetMidY(self.frame)));
    sky.zPosition = 0;
    [self addChild:sky];
    
    // clouds
    SKSpriteNode *clouds = [SKSpriteNode spriteNodeWithImageNamed:@"clouds"];
    clouds.position = CGPointMake(CGRectGetMidX(self.frame), 60);
    clouds.zPosition = 1;
    [self addChild:clouds];
    [clouds runAction:moveCloudsUp];
    
    // fence
    SKSpriteNode *fence = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"fence"]];
    fence.position = CGPointMake(CGRectGetMidX(self.frame), -150);
    fence.zPosition = 1;
    [self addChild:fence];
    [fence runAction:moveSequence];
    
    // game logo
    SKSpriteNode *logo = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"logo"]];
    logo.position = CGPointMake(CGRectGetMidX(self.frame), (CGRectGetMidY(self.frame)));
    logo.name = @"logoImage";
    logo.scale = 0.0;
    logo.zPosition = 3;
    SKAction *delayAppearance = [SKAction waitForDuration:2.0];
    SKAction *scale = [SKAction scaleTo:1.075 duration:0.13];
    SKAction *scaleBack = [SKAction scaleTo:0.9 duration:0.13];
    SKAction *scaleBounce = [SKAction scaleTo:1.00 duration:0.08];
    SKAction *logoSequence = [SKAction sequence:@[delayAppearance, scale, scaleBack, scaleBounce]];
    [self addChild:logo];
    [logo runAction:logoSequence];
    
    SKSpriteNode *pressHere = [SKSpriteNode  spriteNodeWithTexture:[self.textures textureNamed:@"presshere"]];
    pressHere.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    pressHere.zPosition = 4;
    pressHere.name = @"logoImage";
    pressHere.scale = 2.0;
    [self runAction:[SKAction waitForDuration:2.4] completion:^{
        [self addChild:pressHere];
        
        SKAction *scaleUP = [SKAction scaleTo:2.075 duration:0.5];
        SKAction *scaleDOWN = [SKAction scaleTo:1.9 duration:0.5];
        SKAction *scaleSEQUENCE = [SKAction sequence:@[scaleUP, scaleDOWN]];
        [pressHere runAction:[SKAction repeatActionForever:scaleSEQUENCE]];
        
    }];
    
    [self createFlyingTrashArray];
}

- (void)createFlyingTrashArray {
    if (self.flyingTrash) {
        // flying toilet paper
        SKSpriteNode *toiletPaperRight = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"toiletRollRight1"]];
        toiletPaperRight.position = CGPointMake(-200, -200);
        toiletPaperRight.zPosition = 2;
        toiletPaperRight.name = @"paper";
        [self.flyingTrash addObject:toiletPaperRight];
        [self addChild:toiletPaperRight];
        
        // Drumstick
        SKSpriteNode *drumstick = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"drumstick"]];
        drumstick.position = CGPointMake(-200, -200);
        drumstick.zPosition = 2;
        [self.flyingTrash addObject:drumstick];
        [self addChild:drumstick];
        
        // Apple
        SKSpriteNode *apple = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"apple"]];
        apple.position = CGPointMake(-200, -200);
        apple.zPosition = 2;
        [self.flyingTrash addObject:apple];
        [self addChild:apple];
        
        self.FlyingTrashCreated = YES;
    }
}

#pragma mark - Touch Recognisers
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"logoImage"]) {
        [self throwInCards];
        [node runAction:[self fadeOut]];
    }
}

- (SKAction *)fadeOut {
    SKAction *fadeAway = [SKAction fadeOutWithDuration:0.25];
    SKAction *remove = [SKAction removeFromParent];
    SKAction *moveSequence = [SKAction sequence:@[fadeAway, remove]];
    return moveSequence;
}

- (void)throwInCards {
    SKAction *moveIn = [SKAction moveTo:CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)) duration:0.3];
    SKAction *rotate = [SKAction rotateByAngle:arc4random() % 600 duration:0.3];
    
    SKSpriteNode *card1 = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"intro_card"]];
    card1.position = CGPointMake(-500, -500);
    card1.zPosition = 4;
    [self addChild:card1];
    
    SKSpriteNode *card2 = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"intro_card"]];
    card2.position = CGPointMake(1200, -500);
    card2.zPosition = 4;
    [self addChild:card2];
    
    [card1 runAction:[SKAction group:@[rotate, moveIn]] completion:^{
        [card2 runAction:[SKAction sequence:@[[SKAction group:@[rotate.reversedAction, moveIn]], [SKAction waitForDuration:0.3]]] completion:^{
            [self showSplat];
        }];
    }];
}

- (void)showSplat {
    SKSpriteNode *splat = [SKSpriteNode spriteNodeWithTexture:[self.textures textureNamed:@"intro_SPLAT"]];
    splat.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    splat.zPosition = 6;
    [self addChild:splat];
    [self runAction:[SKAction playSoundFileNamed:@"splat.caf" waitForCompletion:NO]];
    [splat runAction:[SKAction waitForDuration:0.5] completion:^{
        [self goToNextScene];
    }];
}

- (void)goToNextScene {
    SKScene *menuScreen = [[MainMenu alloc] initWithSize:self.size];
    SKTransition *fade = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:menuScreen transition:fade];
}

- (void)update:(NSTimeInterval)currentTime {
    if (self.FlyingTrashCreated) {
        for (SKSpriteNode *rubbish in self.flyingTrash) {
            if (arc4random() % 100 == 0) {
                if (!rubbish.hasActions) {
                    if ([rubbish.name isEqualToString:@"paper"]) {
                        NSArray *paperTextures = @[[self.textures textureNamed:@"toiletRollRight1"], [self.textures textureNamed:@"toiletRollRight2"]];
                        SKAction *animate = [SKAction animateWithTextures:paperTextures timePerFrame:0.05];
                        [rubbish runAction:[SKAction group:@[[SKAction repeatAction:animate count:30], [self getMoveSequence]]]];
                        
                    } else {
                        [rubbish runAction:[self getMoveSequence]];
                    }
                }
            }
        }
    }
}

- (SKAction *)getMoveSequence {
    
    CGPathRef path = [self getBezierPathRight];
    SKAction *followPath = [SKAction followPath:path asOffset:NO orientToPath:YES duration:FOLLOW_PATH_SPEED];
    
    return followPath;
}

- (CGPathRef)getBezierPathRight {
        if (self.trashIsMovingLeft) {
            self.trashIsMovingLeft = NO;
            
            UIBezierPath* bezierPath = [UIBezierPath bezierPath];
            [bezierPath moveToPoint: CGPointMake(-114.5-BEZIER_X_SUBTRACTION, 431.5+BEZIER_HEIGHT_ADDITION)];
            [bezierPath addCurveToPoint: CGPointMake(1200.5-BEZIER_X_SUBTRACTION, 431.5+BEZIER_HEIGHT_ADDITION)
                          controlPoint1: CGPointMake(-114.5-BEZIER_X_SUBTRACTION, 431.5+BEZIER_HEIGHT_ADDITION)
                          controlPoint2: CGPointMake(461.12-BEZIER_X_SUBTRACTION, 971.27+BEZIER_HEIGHT_ADDITION)];
            
            return bezierPath.CGPath;
            
        } else {
            self.trashIsMovingLeft = YES;
            
            UIBezierPath* bezierPath = [UIBezierPath bezierPath];
            [bezierPath moveToPoint: CGPointMake(1200.5-BEZIER_X_SUBTRACTION, 431.5+BEZIER_HEIGHT_ADDITION)];
            [bezierPath addCurveToPoint: CGPointMake(-114.5-BEZIER_X_SUBTRACTION, 431.5+BEZIER_HEIGHT_ADDITION)
                          controlPoint1: CGPointMake(1200.5-BEZIER_X_SUBTRACTION, 431.5+BEZIER_HEIGHT_ADDITION)
                          controlPoint2: CGPointMake(624.88-BEZIER_X_SUBTRACTION, 971.27+BEZIER_HEIGHT_ADDITION)];
            
            return bezierPath.CGPath;
        }
    return 0;
}

@end
