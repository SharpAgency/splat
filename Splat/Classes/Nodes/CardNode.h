//
//  CardNode.h
//  Splat
//
//  Created by Beau Young on 30/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Card.h"

@interface CardNode : SKSpriteNode

- (void)loadTextureForCard:(Card *)card;

@end
