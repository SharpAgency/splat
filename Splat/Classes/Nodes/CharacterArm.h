//
//  CharacterArm.h
//  Splat
//
//  Created by Beau Young on 20/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface CharacterArm : SKSpriteNode

- (void)flipCardAction;
- (void)pressSplatAction;

@end
