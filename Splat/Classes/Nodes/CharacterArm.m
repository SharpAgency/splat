//
//  CharacterArm.m
//  Splat
//
//  Created by Beau Young on 20/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "CharacterArm.h"
#import "GameData.h"

#define FOLLOW_PATH_SPEED 0.6

@implementation CharacterArm

#pragma mark - Actions
- (void)flipCardAction {
    [self runAction:[self moveArmForAnimationWithName:@"flipCard"]];
}

- (void)pressSplatAction {
    [self runAction:[self moveArmForAnimationWithName:@"pressSplat"]];
}

- (SKAction *)moveArmForAnimationWithName:(NSString *)animationName {

    if ([animationName isEqualToString:@"flipCard"]) {
        CGPathRef path = [self pathToFollowForCardFlipAnimation];
        SKAction *followPath = [SKAction followPath:path asOffset:NO orientToPath:NO duration:FOLLOW_PATH_SPEED];
        return followPath;
    }
    
    if ([animationName isEqualToString:@"pressSplat"]) {
        CGPathRef path = [self pathToFollowForPressSplatAnimation];
        SKAction *followPath = [SKAction followPath:path asOffset:NO orientToPath:NO duration:FOLLOW_PATH_SPEED];
        return followPath;
    }
    return nil;
}

- (CGPathRef)pathToFollowForCardFlipAnimation {

    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(768.5, 1148.62)];
    [bezierPath addCurveToPoint: CGPointMake(665.5, 917.38) controlPoint1: CGPointMake(768.5, 1148.62) controlPoint2: CGPointMake(671.29, 926.06)];
    [bezierPath addCurveToPoint: CGPointMake(617.21, 1148.62) controlPoint1: CGPointMake(659.71, 908.7) controlPoint2: CGPointMake(489, 969.25)];
    [bezierPath addCurveToPoint: CGPointMake(768.5, 1148.62) controlPoint1: CGPointMake(745.41, 1327.99) controlPoint2: CGPointMake(768.5, 1148.62)];
        
    return bezierPath.CGPath;
}

- (CGPathRef)pathToFollowForPressSplatAnimation {
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(767.5, 1144.62)];
    [bezierPath addCurveToPoint: CGPointMake(664.5, 913.38) controlPoint1: CGPointMake(767.5, 1144.62) controlPoint2: CGPointMake(731.28, 956.54)];
    [bezierPath addCurveToPoint: CGPointMake(231.5, 913.5) controlPoint1: CGPointMake(621.86, 885.81) controlPoint2: CGPointMake(238.58, 902.38)];
    [bezierPath addCurveToPoint: CGPointMake(616.21, 1144.62) controlPoint1: CGPointMake(227.49, 919.8) controlPoint2: CGPointMake(569.86, 1079.78)];
    [bezierPath addCurveToPoint: CGPointMake(767.5, 1144.62) controlPoint1: CGPointMake(744.41, 1323.99) controlPoint2: CGPointMake(767.5, 1144.62)];
    
    return bezierPath.CGPath;
}

@end
