//
//  CardNode.m
//  Splat
//
//  Created by Beau Young on 30/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "CardNode.h"
#import "GameData.h"

@implementation CardNode


- (void)loadTextureForCard:(Card *)card {
    // The deck names are set on the buttons in the shop class
    NSString *suitString;
    switch (card.suit) {
        case SuitBlue:   suitString = @"blue"; break;
        case SuitGreen:  suitString = @"green"; break;
        case SuitRed:    suitString = @"red"; break;
        case SuitYellow: suitString = @"yellow"; break;
    }
    
    NSString *valueString;
    switch (card.value) {
            // Hero Deck
        case blowFly: valueString = @"blowfly"; break;
        case garbageGull: valueString = @"garbagegull"; break;
        case mouldyMilk: valueString = @"mouldymilk"; break;
        case offCheese: valueString = @"offcheese"; break;
        case rottenApple: valueString = @"rottenapple"; break;
        case scumGum: valueString = @"scumgum"; break;
        case smellyFish: valueString = @"smellyfish"; break;
        case sourSnail: valueString = @"soursnail"; break;
        case trashapillar: valueString = @"trashapillar"; break;
        case trashola: valueString = @"trashola"; break;
        case trashRat: valueString = @"trashrat"; break;
        case wastedBanana: valueString = @"wastedbanana"; break;
            
            // Series 1 Deck
        case garbageegg: valueString = @"garbageegg"; break;
        case germit: valueString = @"germit"; break;
        case germworm: valueString = @"germworm"; break;
        case grottdog: valueString = @"grottdog"; break;
        case kingrat: valueString = @"kingrat"; break;
        case loopaper: valueString = @"loopaper"; break;
        case mangler: valueString = @"mangler"; break;
        case moocowdisease: valueString = @"moocowdisease"; break;
        case muckymaggot: valueString = @"muckymaggot"; break;
        case noxiousbee: valueString = @"noxiousbee"; break;
        case pukepod: valueString = @"pukepod"; break;
        case putridboot: valueString = @"putridboot"; break;
        case putridpizza: valueString = @"putridpizza"; break;
        case putridsardine: valueString = @"putridsardine"; break;
        case rancidroach: valueString = @"rancidroach"; break;
        case rancidsteak: valueString = @"rancidsteak"; break;
        case rankenstein: valueString = @"rankenstein"; break;
        case rotmoth: valueString = @"rotmoth"; break;
        case rottbox: valueString = @"rottbox"; break;
        case scummyscreen: valueString = @"scummyscreen"; break;
        case scummysquirrel: valueString = @"scummysquirrel"; break;
        case sickenchicken: valueString = @"sickenchicken"; break;
        case slopcorn: valueString = @"slopcorn"; break;
        case smellysox: valueString = @"smellysox"; break;
        case snotten: valueString = @"snotten"; break;
        case soggytomato: valueString = @"soggytomato"; break;
        case stalebread: valueString = @"stalebread"; break;
        case stenchfries: valueString = @"stenchfries"; break;
        case stinkysoda: valueString = @"stinkysoda"; break;
        case toxictrashrim: valueString = @"toxictrashrim"; break;
        case trashcat: valueString = @"trashcat"; break;
        case wastedwolf: valueString = @"wastedwolf"; break;
        case awfulpie: valueString = @"awfulpie"; break;
        case bashedbottle: valueString = @"bashedbottle"; break;
        case binscabbeetle: valueString = @"binscabbeetle"; break;
        case birdflu: valueString = @"birdflu"; break;
        case chickypox: valueString = @"chickypox"; break;
        case compostmonster: valueString = @"compostmonster"; break;
        case crudcan: valueString = @"crudcan"; break;
        case disinfectant: valueString = @"disinfectant"; break;
        case dumpling: valueString = @"dumpling"; break;
        case flesheatingvirus: valueString = @"flesheatingvirus"; break;
        case fluvirus: valueString = @"fluvirus"; break;
        case foulnugget: valueString = @"foulnugget"; break;
            
            // Series 2 Deck
        case alleygator: valueString = @"alleygator"; break;
        case binfire: valueString = @"binfire"; break;
        case boggybagel: valueString = @"boggybagel"; break;
        case boghog: valueString = @"boghog"; break;
        case bumbug: valueString = @"bumbug"; break;
        case burny: valueString = @"burny"; break;
        case ceryill: valueString = @"ceryill"; break;
        case compost: valueString = @"compost"; break;
        case cukoo: valueString = @"cukoo"; break;
        case dripzy: valueString = @"dripzy"; break;
        case dungbug: valueString = @"dungbug"; break;
        case garbagebear: valueString = @"garbagebear"; break;
        case grossgoose: valueString = @"grossgoose"; break;
        case grottrocket: valueString = @"grottrocket"; break;
        case grottyfruity: valueString = @"grottyfruity"; break;
        case grubby: valueString = @"grubby"; break;
        case gunkygargoyle: valueString = @"gunkygargoyle"; break;
        case ickystinger: valueString = @"ickystinger"; break;
        case joosed: valueString = @"joosed"; break;
        case junkclops: valueString = @"junkclops"; break;
        case junkjumper: valueString = @"junkjumper"; break;
        case lugger: valueString = @"lugger"; break;
        case melter: valueString = @"melter"; break;
        case mouldshake: valueString = @"mouldshake"; break;
        case nutso: valueString = @"nutso"; break;
        case poopew: valueString = @"poopew"; break;
        case putridpretzel: valueString = @"putridpretzel"; break;
        case rankoon: valueString = @"rankoon"; break;
        case rottensandwich: valueString = @"rottensandwich"; break;
        case scummymummy: valueString = @"scummymummy"; break;
        case septix: valueString = @"septix"; break;
        case skweevil: valueString = @"skweevil"; break;
        case sludger: valueString = @"sludger"; break;
        case sludgeslug: valueString = @"sludgeslug"; break;
        case soreberry: valueString = @"soreberry"; break;
        case spewghetti: valueString = @"spewghetti"; break;
        case splider: valueString = @"splider"; break;
        case squirmer: valueString = @"squirmer"; break;
        case squishfish: valueString = @"squishfish"; break;
        case stingle: valueString = @"stingle"; break;
        case sudz: valueString = @"sudz"; break;
        case trashbaggoblin: valueString = @"trashbaggoblin"; break;
        case trashcraft: valueString = @"trashcraft"; break;
        case trashmite: valueString = @"trashmite"; break;
        case trashtube: valueString = @"trashtube"; break;
        case unladybug: valueString = @"unladybug"; break;
        case vilehifi: valueString = @"vilehifi"; break;
        case vomster: valueString = @"vomster"; break;
        case vulgore: valueString = @"vulgore"; break;
        case yuckduck: valueString = @"yuckduck"; break;
        case yuckotaco: valueString = @"yuckotaco"; break;
        case zapper: valueString = @"zapper"; break;
        case trashellite: valueString = @"trashellite"; break;
        
            // Series 3 Deck
        case badant: valueString = @"badant"; break;
        case barfbq: valueString = @"barfbq"; break;
        case binbros: valueString = @"binbros"; break;
        case binbrute: valueString = @"binbrute"; break;
        case blobmoz: valueString = @"blobmoz"; break;
        case brokenblender: valueString = @"brokenblender"; break;
        case burpyslurpy: valueString = @"burpyslurpy"; break;
        case cackycake: valueString = @"cackycake"; break;
        case cloggedcamera: valueString = @"cloggedcamera"; break;
        case cringechilli: valueString = @"cringechilli"; break;
        case cruddykebab: valueString = @"cruddykebab"; break;
        case dankdishwasher: valueString = @"dankdishwasher"; break;
        case feralfly: valueString = @"feralfly"; break;
        case feralfridge: valueString = @"feralfridge"; break;
        case foulfishbowl: valueString = @"foulfishbowl"; break;
        case gasghost: valueString = @"gasghost"; break;
        case grimygold: valueString = @"grimygold"; break;
        case grotpot: valueString = @"grotpot"; break;
        case grottybotfly: valueString = @"grottybotfly"; break;
        case grottycoffee: valueString = @"grottycoffee"; break;
        case gunkpumpkin: valueString = @"gunkpumpkin"; break;
        case ickytick: valueString = @"ickytick"; break;
        case junkjewellery: valueString = @"junkjewellery"; break;
        case junkmail: valueString = @"junkmail"; break;
        case kruddykennel: valueString = @"kruddykennel"; break;
        case lochmess: valueString = @"lochmess"; break;
        case lunkosaur: valueString = @"lunkosaur"; break;
        case maggotmeatball: valueString = @"maggotmeatball"; break;
        case oilyointment: valueString = @"oilyointment"; break;
        case oozeogre: valueString = @"oozeogre"; break;
        case pestyparasite: valueString = @"pestyparasite"; break;
        case poopmonster: valueString = @"poopmonster"; break;
        case pootato: valueString = @"pootato"; break;
        case pusplop: valueString = @"pusplop"; break;
        case rockroach: valueString = @"rockroach"; break;
        case rottenroll: valueString = @"rottenroll"; break;
        case rottenrollerblade: valueString = @"rottenrollerblade"; break;
        case rottypop: valueString = @"rottypop"; break;
        case rotvault: valueString = @"rotvault"; break;
        case scaboon: valueString = @"scaboon"; break;
        case shockoli: valueString = @"shockoli"; break;
        case sickgarlic: valueString = @"sickgarlic"; break;
        case sicklysweets: valueString = @"sicklysweets"; break;
        case sicktoc: valueString = @"sicktoc"; break;
        case skabbyshark: valueString = @"skabbyshark"; break;
        case smellyonion: valueString = @"smellyonion"; break;
        case spewster: valueString = @"spewster"; break;
        case stickypop: valueString = @"stickypop"; break;
        case trashantula: valueString = @"trashantula"; break;
        case trashcash: valueString = @"trashcash"; break;
        case trashiron: valueString = @"trashiron"; break;
        case trashytorch: valueString = @"trashytorch"; break;
        case uglybedbug: valueString = @"uglybedbug"; break;
        case wasteworm: valueString = @"wasteworm"; break;
        case yuckketchup: valueString = @"yuckketchup"; break;
            
            // Series 4 Deck
        case badanchovy: valueString = @"badanchovy"; break;
        case bangedupgamer: valueString = @"bangedupgamer"; break;
        case barfbagette: valueString = @"barfbagette"; break;
        case boiledbrains: valueString = @"boiledbrains"; break;
        case burriedburrito: valueString = @"burriedburrito"; break;
        case carnagecar: valueString = @"carnagecar"; break;
        case cruddycurry: valueString = @"cruddycurry"; break;
        case deadybear: valueString = @"deadybear"; break;
        case dumpeddryer: valueString = @"dumpeddryer"; break;
        case dustyrug: valueString = @"dustyrug"; break;
        case filthyfan: valueString = @"filthyfan"; break;
        case fishyfinger: valueString = @"fishyfinger"; break;
        case gooeysouvlaki: valueString = @"gooeysouvlaki"; break;
        case grimribs: valueString = @"grimribs"; break;
        case gristlepie: valueString = @"gristlepie"; break;
        case grossgoulash: valueString = @"grossgoulash"; break;
        case grothopper: valueString = @"grothopper"; break;
        case gunkasaur: valueString = @"gunkasaur"; break;
        case gutterfly: valueString = @"gutterfly"; break;
        case gutterskater: valueString = @"gutterskater"; break;
        case hackedham: valueString = @"hackedham"; break;
        case horridhaggis: valueString = @"horridhaggis"; break;
        case kackykettle: valueString = @"kackykettle"; break;
        case lumpyyoghurt: valueString = @"lumpyyoghurt"; break;
        case mashedmeat: valueString = @"mashedmeat"; break;
        case moldymattress: valueString = @"moldymattress"; break;
        case moldymelon: valueString = @"moldymelon"; break;
        case muckyducky: valueString = @"muckyducky"; break;
        case mushypeas: valueString = @"mushypeas"; break;
        case oozeynoodles: valueString = @"oozeynoodles"; break;
        case pearbeware: valueString = @"pearbeware"; break;
        case zappedlamp: valueString = @"zappedlamp"; break;
        case scarredcaviar: valueString = @"scarredcaviar"; break;
        case shabbycabbage: valueString = @"shabbycabbage"; break;
        case shockinghorse: valueString = @"shockinghorse"; break;
        case slimeapple: valueString = @"slimeapple"; break;
        case slimobeetle: valueString = @"slimobeetle"; break;
        case sludgeylasagne: valueString = @"sludgeylasagne"; break;
        case smashedvase: valueString = @"smashedvase"; break;
        case smearwig: valueString = @"smearwig"; break;
        case sneezeburger: valueString = @"sneezeburger"; break;
        case snotsnail: valueString = @"snotsnail"; break;
        case spewystew: valueString = @"spewystew"; break;
        case splatslater: valueString = @"splatslater"; break;
        case stuffedchicken: valueString = @"stuffedchicken"; break;
        case vilevacuum: valueString = @"vilevacuum"; break;
        case wastetruck: valueString = @"wastetruck"; break;
        case yuckyo: valueString = @"yuckyo"; break;
        case yuckysprout: valueString = @"yuckysprout"; break;
        
        
            // Series 5 Deck
        case yuckbox: valueString = @"yuckbox"; break;
        case awfulalgae: valueString = @"awfulalgae"; break;
        case bogbag: valueString = @"bogbag"; break;
        case boggyborscht: valueString = @"boggyborscht"; break;
        case bustedbox: valueString = @"bustedbox"; break;
        case carrot: valueString = @"carrot"; break;
        case chilledchicken: valueString = @"chilledchicken"; break;
        case cloggedupcalculator: valueString = @"cloggedupcalculator"; break;
        case coldpizza: valueString = @"coldpizza"; break;
        case coldprawn: valueString = @"coldprawn"; break;
        case crackedupcarton: valueString = @"crackedupcarton"; break;
        case dankdonut: valueString = @"dankdonut"; break;
        case doodoodiaper: valueString = @"doodoodiaper"; break;
        case driedrice: valueString = @"driedrice"; break;
        case filthyfroglegs: valueString = @"filthyfroglegs"; break;
        case fishandslops: valueString = @"fishandslops"; break;
        case friedant: valueString = @"friedant"; break;
        case frostfish: valueString = @"frostfish"; break;
        case frozenfly: valueString = @"frozenfly"; break;
        case furryturkishdelight: valueString = @"furryturkishdelight"; break;
        case gakkymacaron: valueString = @"gakkymacaron"; break;
        case googlue: valueString = @"googlue"; break;
        case grossmoose: valueString = @"grossmoose"; break;
        case grottylambchop: valueString = @"grottylambchop"; break;
        case hairlice: valueString = @"hairlice"; break;
        case ickypickle: valueString = @"ickypickle"; break;
        case littlelocker: valueString = @"littlelocker"; break;
        case moldysandwich: valueString = @"moldysandwich"; break;
        case mushymango: valueString = @"mushymango"; break;
        case oldnews: valueString = @"oldnews"; break;
        case oozeyjooze: valueString = @"oozeyjooze"; break;
        case pestylocust: valueString = @"pestylocust"; break;
        case pukecakes: valueString = @"pukecakes"; break;
        case queasycheese: valueString = @"queasycheese"; break;
        case saverkrout: valueString = @"saverkrout"; break;
        case scrappack: valueString = @"scrappack"; break;
        case sewagesupreme: valueString = @"sewagesupreme"; break;
        case sewerpipe: valueString = @"sewerpipe"; break;
        case slimyspitball: valueString = @"slimyspitball"; break;
        case snottyschnitzel: valueString = @"snottyschnitzel"; break;
        case spewbluecheese: valueString = @"spewbluecheese"; break;
        case spittlewhistle: valueString = @"spittlewhistle"; break;
        case squashedmaggot: valueString = @"squashedmaggot"; break;
        case squirmcake: valueString = @"squirmcake"; break;
        case stinkspray: valueString = @"stinkspray"; break;
        case stinkyring: valueString = @"stinkyring"; break;
        case stuffedpencilcase: valueString = @"stuffedpencilcase"; break;
        case trashtop: valueString = @"trashtop"; break;
        case trashytextbook: valueString = @"trashytextbook"; break;
        case turdturtle: valueString = @"turdturtle"; break;

            // Series 6 Deck
        case wheelygross: valueString = @"wheelygross"; break;
        case axidentalaxe: valueString = @"axidentalaxe"; break;
        case bakedbug: valueString = @"bakedbug"; break;
        case batteredbat: valueString = @"batteredbat"; break;
        case bilebison: valueString = @"bilebison"; break;
        case boggybonfire: valueString = @"boggybonfire"; break;
        case boneage: valueString = @"boneage"; break;
        case cavebooger: valueString = @"cavebooger"; break;
        case cavedecay: valueString = @"cavedecay"; break;
        case dinodregg: valueString = @"dinodregg"; break;
        case dinopoop: valueString = @"dinopoop"; break;
        case doodoobird: valueString = @"doodoobird"; break;
        case dreggleggs: valueString = @"dreggleggs"; break;
        case eggosaur: valueString = @"eggosaur"; break;
        case eggyelephant: valueString = @"eggyelephant"; break;
        case filthifiedfish: valueString = @"filthifiedfish"; break;
        case flintrockfish: valueString = @"flintrockfish"; break;
        case flushosaurus: valueString = @"flushosaurus"; break;
        case flyceratops: valueString = @"flyceratops"; break;
        case grimeygull: valueString = @"grimeygull"; break;
        case hardboiledbuzzer: valueString = @"hardboiledbuzzer"; break;
        case lumpylobster: valueString = @"lumpylobster"; break;
        case muckymite: valueString = @"muckymite"; break;
        case oozyoctopus: valueString = @"oozyoctopus"; break;
        case poachedspider: valueString = @"poachedspider"; break;
        case pukasaur: valueString = @"pukasaur"; break;
        case pukingprimate: valueString = @"pukingprimate"; break;
        case ratosaurus: valueString = @"ratosaurus"; break;
        case rottentoothtiger: valueString = @"rottentoothtiger"; break;
        case rubbishreptile: valueString = @"rubbishreptile"; break;
        case runnyroach: valueString = @"runnyroach"; break;
        case scrambledrat: valueString = @"scrambledrat"; break;
        case scumscorcher: valueString = @"scumscorcher"; break;
        case sewageslug: valueString = @"sewageslug"; break;
        case sewerslither: valueString = @"sewerslither"; break;
        case sicklyslug: valueString = @"sicklyslug"; break;
        case smellerops: valueString = @"smellerops"; break;
        case smellyjellyfish: valueString = @"smellyjellyfish"; break;
        case snotadactyl: valueString = @"snotadactyl"; break;
        case softboiledsloth: valueString = @"softboiledsloth"; break;
        case soiledshrimp: valueString = @"soiledshrimp"; break;
        case stinkybone: valueString = @"stinkybone"; break;
        case trashosaurus: valueString = @"trashosaurus"; break;
        case trashraptor: valueString = @"trashraptor"; break;
        case twrecks: valueString = @"twrecks"; break;
        case uglybug: valueString = @"uglybug"; break;
        case wastedwasp: valueString = @"wastedwasp"; break;
    }

    SKAction *changetexture = [SKAction setTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"%@_%@.png", valueString, suitString]]];
    NSLog(@"Drawn Card: %@_%@", valueString, suitString);
    [self runAction:changetexture];
    
    NSArray *specialCardNames = @[@"soggytomato",
                                  @"trashcat",
                                  @"kingrat",
                                  @"trashcraft",
                                  @"trashellite",
                                  @"grottrocket",
                                  @"grimygold",
                                  @"junkjewellery",
                                  @"rotvault",
                                  @"sicktoc",
                                  @"trashcash"];
    
    for (NSString *string in specialCardNames) {
        if ([valueString isEqualToString:string]) {
            [[GameData sharedData] setIsSpecialCard:YES];
        }
    }
}

@end
