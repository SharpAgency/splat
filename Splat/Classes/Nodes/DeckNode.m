//
//  DeckNode.m
//  Splat
//
//  Created by Beau Young on 31/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "DeckNode.h"

@implementation DeckNode {
    SKAction *scaleSequence;
}

- (void)startPulse {
    SKAction *scaleUp = [SKAction scaleTo:1.05 duration:0.7];
    SKAction *scaleDown = [SKAction scaleTo:0.95 duration:0.7];
    scaleSequence = [SKAction sequence:@[scaleUp, scaleDown]];
    
    [self runAction:[SKAction repeatActionForever:scaleSequence]];
}

- (void)stopPulse {
    [self removeAllActions];
    [self runAction:[SKAction scaleTo:1.0 duration:0.2]];
}

@end
