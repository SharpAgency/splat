//
//  DeckNode.h
//  Splat
//
//  Created by Beau Young on 31/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface DeckNode : SKSpriteNode

- (void)startPulse;
- (void)stopPulse;

@end
