//
//  GameData.m
//  Splat
//
//  Created by Beau Young on 18/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "GameData.h"

@interface GameData()

@end

@implementation GameData

#pragma mark - init
+ (GameData *)sharedData {
    static dispatch_once_t once;
    static GameData *sharedData = nil;

    dispatch_once(&once, ^{
        sharedData = [[GameData alloc] init];
    });
    return sharedData;
}

- (id)init {
    if (self = [super init]) {
        _musicLevel = [[NSUserDefaults standardUserDefaults] floatForKey:@"musicLevel"];
        _soundFXLevel = [[NSUserDefaults standardUserDefaults] floatForKey:@"soundFXLevel"];
        _difficultyLevel = [[NSUserDefaults standardUserDefaults] floatForKey:@"difficultyLevel"];
        _selectedBackgroundIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedBackgroundIndex"];
        _selectedTrashieName = [[NSUserDefaults standardUserDefaults] stringForKey:@"selectedTrashie"];
        _selectedDeckName = [[NSUserDefaults standardUserDefaults] stringForKey:@"selectedDeck"];

        _purchasedDecks = [[NSArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"purchasedDecks"]] mutableCopy];
    }
    return self;
}

#pragma mark - Options
- (NSString *)getBackgroundImageName {
    NSString *backgroundName;
    if (_selectedBackgroundIndex == 0) {
        backgroundName = @"dingydiner.jpg";
        [self setupBackgroundMusicWithFileName:@"diner" ofType:@"aif"];
    }
    else if (_selectedBackgroundIndex == 1) {
        backgroundName = @"crudbeach.jpg";
        [self setupBackgroundMusicWithFileName:@"beach" ofType:@"aif"];
    }
    else if (_selectedBackgroundIndex == 2) {
        backgroundName = @"sewerhole.jpg";
        [self setupBackgroundMusicWithFileName:@"sewer" ofType:@"aif"];
    }
    else if (_selectedBackgroundIndex == 3) {
        backgroundName = @"scumalley.jpg";
        [self setupBackgroundMusicWithFileName:@"alley" ofType:@"aif"];
    }
    return backgroundName;
}

- (void)setupBackgroundMusicWithFileName:(NSString *)fileName ofType:(NSString *)type {
    
    NSString *bgSoundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:type];
    NSURL *bgFileURL = [[NSURL alloc] initFileURLWithPath:bgSoundFilePath];
    
    _bgMusic = [[AVAudioPlayer alloc] initWithContentsOfURL:bgFileURL error:nil];
    _bgMusic.numberOfLoops = -1; // inifinite loop
    _bgMusic.volume = _musicLevel;
    [self.bgMusic play];
}

- (float)getDifficultyDelay {
    return _difficultyLevel+0.5;
}

#pragma mark - Setters
- (void)setMusicLevel:(float)musicLevel {
    [[NSUserDefaults standardUserDefaults] setFloat:musicLevel forKey:@"musicLevel"];
    _musicLevel = musicLevel;
    NSLog(@"volume level: %f", musicLevel);
    _bgMusic.volume = _musicLevel;
}

- (void)setSoundFXLevel:(float)soundFXLevel {
    [[NSUserDefaults standardUserDefaults] setFloat:soundFXLevel forKey:@"soundFXLevel"];
    _soundFXLevel = soundFXLevel;
}

- (void)setDifficultyLevel:(float)difficultyLevel {
    [[NSUserDefaults standardUserDefaults] setFloat:difficultyLevel forKey:@"difficultyLevel"];
    NSLog(@"difficulty level: %f", difficultyLevel);
    _difficultyLevel = difficultyLevel;
}

- (void)setSelectedBackgroundIndex:(NSInteger)selectedBackgroundIndex {
    [[NSUserDefaults standardUserDefaults] setInteger:selectedBackgroundIndex forKey:@"selectedBackgroundIndex"];
    _selectedBackgroundIndex = selectedBackgroundIndex;
}

- (void)setSelectedTrashie:(NSString *)selectedTrashie {
    [[NSUserDefaults standardUserDefaults] setObject:selectedTrashie forKey:@"selectedTrashie"];
    _selectedTrashieName = selectedTrashie;
}

- (void)setSelectedDeckName:(NSString *)selectedDeckName {
    [[NSUserDefaults standardUserDefaults] setObject:selectedDeckName forKey:@"selectedDeck"];
    _selectedDeckName = selectedDeckName;
}

- (void)setPurchasedDeckName:(NSString *)purchasedDeckName {
    [_purchasedDecks addObject:purchasedDeckName];
    [[NSUserDefaults standardUserDefaults] setObject:_purchasedDecks forKey:@"purchasedDecks"];
}



@end
