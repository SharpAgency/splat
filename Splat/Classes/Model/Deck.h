//
//  Deck.h
//  Splat
//
//  Created by Beau Young on 14/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Card;

@interface Deck : NSObject

- (void)setUpCards;
- (void)shuffle;
- (Card *)draw;
- (int)cardsRemaining;

@end
