//
//  SplatIAPHelper.h
//  Splat
//
//  Created by Beau Young on 10/02/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "IAPHelper.h"

@interface SplatIAPHelper : IAPHelper

+ (SplatIAPHelper *)sharedInstance;

@end
