//
//  SplatIAPHelper.m
//  Splat
//
//  Created by Beau Young on 10/02/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "SplatIAPHelper.h"

@implementation SplatIAPHelper

+ (SplatIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static SplatIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                     @"com.sharpagency.splat.seriesOneDeckPurchase",
                                     @"com.sharpagency.splat.seriesTwoDeckIAP",
                                     @"com.sharpagency.splat.seriesThreeDeckIAP",
                                     @"com.sharpagency.splat.seriesFourDeckIAP",
                                     @"com.sharpagency.splat.seriesFiveDeckIAP",
                                     @"com.sharpagency.splat.seriesSixDeckIAP",
                                     nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

@end
