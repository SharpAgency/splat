//
//  GameData.h
//  Splat
//
//  Created by Beau Young on 18/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface GameData : NSObject

@property (nonatomic) float musicLevel;
@property (nonatomic) float soundFXLevel;
@property (nonatomic) float difficultyLevel;
@property (nonatomic) NSInteger selectedBackgroundIndex;

@property (strong, nonatomic) NSString *selectedTrashieName;
@property (strong, nonatomic) NSString *selectedDeckName;
@property (strong, nonatomic) NSString *purchasedDeckName;
@property (strong, nonatomic) NSMutableArray *purchasedDecks;

@property (nonatomic) BOOL isSpecialCard;

@property (nonatomic) BOOL isPlayWithTrashie;

@property (strong, nonatomic) AVAudioPlayer *bgMusic;

+ (GameData *)sharedData;

- (NSString *)getBackgroundImageName;
- (float)getDifficultyDelay;
- (void)setupBackgroundMusicWithFileName:(NSString *)fileName ofType:(NSString *)type;

@end