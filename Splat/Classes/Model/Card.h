//
//  Card.h
//  Splat
//
//  Created by Beau Young on 14/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	SuitBlue,
	SuitGreen,
	SuitRed,
	SuitYellow,
}
Suit;
// Hero Deck
#define blowFly 1
#define garbageGull 2
#define mouldyMilk 3
#define offCheese 4
#define rottenApple 5
#define scumGum 6
#define smellyFish 7
#define sourSnail 8
#define trashapillar 9
#define trashola 10
#define trashRat 11
#define wastedBanana 12

// Series 1 Deck
#define garbageegg 13
#define germit 14
#define germworm 15
#define grottdog 16
#define kingrat 17
#define loopaper 18
#define mangler 19
#define moocowdisease 20
#define muckymaggot 21
#define noxiousbee 22
#define pukepod 23
#define putridboot 24
#define putridpizza 25
#define putridsardine 26
#define rancidroach 27
#define rancidsteak 28
#define rankenstein 29
#define rotmoth 30
#define rottbox 31
#define scummyscreen 32
#define scummysquirrel 33
#define sickenchicken 34
#define slopcorn 35
#define smellysox 36
#define snotten 37
#define soggytomato 38
#define stalebread 39
#define stenchfries 40
#define stinkysoda 41
#define toxictrashrim 42
#define trashcat 43
#define wastedwolf 44
#define awfulpie 45
#define bashedbottle 46
#define binscabbeetle 47
#define birdflu 48
#define chickypox 49
#define compostmonster 50
#define crudcan 51
#define disinfectant 52
#define dumpling 53
#define flesheatingvirus 54
#define fluvirus 55
#define foulnugget 56

// Series 2 Deck
#define alleygator 57
#define binfire 58
#define boggybagel 59
#define boghog 60
#define bumbug 61
#define burny 62
#define ceryill 63
#define compost 64
#define cukoo 65
#define dripzy 66
#define dungbug 67
#define garbagebear 68
#define grossgoose 69
#define grottrocket 70
#define grottyfruity 71
#define grubby 72
#define gunkygargoyle 73
#define ickystinger 74
#define joosed 75
#define junkclops 76
#define junkjumper 77
#define lugger 78
#define melter 79
#define mouldshake 80
#define nutso 81
#define poopew 82
#define putridpretzel 83
#define rankoon 84
#define rottensandwich 85
#define scummymummy 86
#define septix 87
#define skweevil 88
#define sludger 89
#define sludgeslug 90
#define soreberry 91
#define spewghetti 92
#define splider 93
#define squirmer 94
#define squishfish 95
#define stingle 96
#define sudz 97
#define trashbaggoblin 98
#define trashcraft 99
#define trashmite 100
#define trashtube 101
#define unladybug 102
#define vilehifi 103
#define vomster 104
#define vulgore 105
#define yuckduck 106
#define yuckotaco 107
#define zapper 108
#define trashellite 109

// Series 3 Deck
#define barfbq 110
#define binbros 111
#define binbrute 112
#define blobmoz 113
#define brokenblender 114
#define burpyslurpy 115
#define cackycake 116
#define cloggedcamera 117
#define cringechilli 118
#define cruddykebab 119
#define dankdishwasher 120
#define feralfly 121
#define feralfridge 122
#define foulfishbowl 123
#define gasghost 124
#define grimygold 125
#define grotpot 126
#define grottybotfly 127
#define grottycoffee 128
#define gunkpumpkin 129
#define ickytick 130
#define junkjewellery 131
#define junkmail 132
#define kruddykennel 133
#define lochmess 134
#define lunkosaur 135
#define maggotmeatball 136
#define oilyointment 137
#define oozeogre 138
#define pestyparasite 139
#define poopmonster 140
#define pootato 141
#define pusplop 142
#define rockroach 143
#define rottenroll 144
#define rottenrollerblade 145
#define rottypop 146
#define rotvault 147
#define scaboon 148
#define shockoli 149
#define sickgarlic 150
#define sicklysweets 151
#define sicktoc 152
#define skabbyshark 153
#define smellyonion 154
#define spewster 155
#define stickypop 156
#define trashantula 157
#define trashcash 158
#define trashiron 159
#define trashytorch 160
#define uglybedbug 161
#define wasteworm 162
#define yuckketchup 163
#define badant 164


// Series 4 Deck
#define bangedupgamer 165
#define barfbagette 166
#define boiledbrains 167
#define burriedburrito 168
#define carnagecar 169
#define cruddycurry 170
#define deadybear 171
#define dumpeddryer 172
#define dustyrug 173
#define filthyfan 174
#define fishyfinger 175
#define gooeysouvlaki 176
#define grimribs 177
#define gristlepie 178
#define grossgoulash 179
#define grothopper 180
#define gunkasaur 181
#define gutterfly 182
#define gutterskater 183
#define hackedham 184
#define horridhaggis 185
#define kackykettle 186
#define lumpyyoghurt 187
#define mashedmeat 188
#define moldymattress 189
#define moldymelon 190
#define muckyducky 191
#define mushypeas 192
#define oozeynoodles 193
#define pearbeware 194
#define zappedlamp 195
#define scarredcaviar 196
#define shabbycabbage 197
#define shockinghorse 198
#define slimeapple 199
#define slimobeetle 200
#define sludgeylasagne 201
#define smashedvase 202
#define smearwig 203
#define sneezeburger 204
#define snotsnail 205
#define spewystew 206
#define splatslater 207
#define stuffedchicken 208
#define vilevacuum 209
#define wastetruck 210
#define yuckyo 211
#define yuckysprout 212
#define badanchovy 213

// Series 5 Deck
#define awfulalgae 214
#define bogbag 215
#define boggyborscht 216
#define bustedbox 217
#define carrot 218
#define chilledchicken 219
#define cloggedupcalculator 220
#define coldpizza 221
#define coldprawn 222
#define crackedupcarton 223
#define dankdonut 224
#define doodoodiaper 225
#define driedrice 226
#define filthyfroglegs 227
#define fishandslops 228
#define yuckbox 229
#define friedant 230
#define frostfish 231
#define frozenfly 232
#define furryturkishdelight 233
#define gakkymacaron 234
#define googlue 235
#define grossmoose 236
#define grottylambchop 237
#define turdturtle 238
#define hairlice 239
#define ickypickle 240
#define littlelocker 241
#define moldysandwich 242
#define mushymango 243
#define oldnews 244
#define oozeyjooze 245
#define pestylocust 246
#define trashytextbook 247
#define trashtop 248
#define pukecakes 249
#define queasycheese 250
#define saverkrout 251
#define scrappack 252
#define sewagesupreme 253
#define sewerpipe 254
#define slimyspitball 255
#define snottyschnitzel 256
#define spewbluecheese 257
#define stuffedpencilcase 258
#define spittlewhistle 259
#define squashedmaggot 260
#define squirmcake 261
#define stinkspray 262
#define stinkyring 263

// Series 6 Deck
#define wheelygross 264
#define trashraptor 265
#define twrecks 266
#define uglybug 267
#define wastedwasp 268
#define axidentalaxe 269
#define bakedbug 270
#define batteredbat 271
#define bilebison 272
#define boggybonfire 273
#define boneage 274
#define cavebooger 275
#define cavedecay 276
#define dinodregg 277
#define dinopoop 278
#define doodoobird 279
#define dreggleggs 280
#define eggosaur 281
#define eggyelephant 282
#define filthifiedfish 283
#define flintrockfish 284
#define flushosaurus 285
#define flyceratops 286
#define grimeygull 287
#define hardboiledbuzzer 288
#define lumpylobster 289
#define muckymite 290
#define oozyoctopus 291
#define poachedspider 292
#define pukasaur 293
#define pukingprimate 294
#define ratosaurus 295
#define rottentoothtiger 296
#define rubbishreptile 297
#define runnyroach 298
#define scrambledrat 299
#define scumscorcher 300
#define sewageslug 301
#define sewerslither 302
#define sicklyslug 303
#define smellerops 304
#define smellyjellyfish 305
#define snotadactyl 306
#define softboiledsloth 307
#define soiledshrimp 308
#define stinkybone 309
#define trashosaurus 310

@interface Card : NSObject

@property (nonatomic, readonly) Suit suit;
@property (nonatomic, readonly) int value;

- (id)initWithSuit:(Suit)suit value:(int)value;

@end
