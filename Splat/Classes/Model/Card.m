//
//  Card.m
//  Splat
//
//  Created by Beau Young on 14/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "Card.h"

@interface Card()

@end

@implementation Card

- (id)initWithSuit:(Suit)suit value:(int)value {
    NSAssert(value >= blowFly && value <= trashosaurus, @"Invalid card value");
    
    if ((self = [super init])) {
        _suit = suit;
        _value = value;
    }
    return self;
}

@end
