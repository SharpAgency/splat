//
//  Deck.m
//  Splat
//
//  Created by Beau Young on 14/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "Deck.h"
#import "Card.h"
#import "GameData.h"

static NSString *seriesOneIdentifier = @"com.sharpagency.splat.seriesOneDeckPurchaseDeck";
static NSString *seriesTwoIdentifier = @"com.sharpagency.splat.seriesTwoDeckIAPDeck";
static NSString *seriesThreeIdentifier = @"com.sharpagency.splat.seriesThreeDeckIAPDeck";
static NSString *seriesFourIdentifier = @"com.sharpagency.splat.seriesFourDeckIAPDeck";
static NSString *seriesFiveIdentifier = @"com.sharpagency.splat.seriesFiveDeckIAPDeck";
static NSString *seriesSixIdentifier = @"com.sharpagency.splat.seriesSixDeckIAPDeck";

@implementation Deck {
    NSMutableArray *_cards;
}

- (void)setUpCards {
    for (Suit suit = SuitBlue; suit <= SuitYellow; ++suit) {
        if ([[GameData sharedData].selectedDeckName isEqualToString:@"heroDeck"]) {
            for (int value = blowFly; value <= wastedBanana; ++value) {
                Card *card = [[Card alloc] initWithSuit:suit value:value];
                [_cards addObject:card];
            }
        }
        else if ([[GameData sharedData].selectedDeckName isEqualToString:seriesOneIdentifier]) {
            for (int value = garbageegg; value <= foulnugget; ++value) {
                Card *card = [[Card alloc] initWithSuit:suit value:value];
                [_cards addObject:card];
            }
        }
        else if ([[GameData sharedData].selectedDeckName isEqualToString:seriesTwoIdentifier]) {
            for (int value = alleygator; value <= trashellite; ++value) {
                Card *card = [[Card alloc] initWithSuit:suit value:value];
                [_cards addObject:card];
            }
        }
        else if ([[GameData sharedData].selectedDeckName isEqualToString:seriesThreeIdentifier]) {
            for (int value = barfbq; value <= badant; ++value) {
                Card *card = [[Card alloc] initWithSuit:suit value:value];
                [_cards addObject:card];
            }
        }
        else if ([[GameData sharedData].selectedDeckName isEqualToString:seriesFourIdentifier]) {
            for (int value = bangedupgamer; value <= badanchovy; ++value) {
                Card *card = [[Card alloc] initWithSuit:suit value:value];
                [_cards addObject:card];
            }
        }
        else if ([[GameData sharedData].selectedDeckName isEqualToString:seriesFiveIdentifier]) {
            for (int value = awfulalgae; value <= stinkyring; ++value) {
                Card *card = [[Card alloc] initWithSuit:suit value:value];
                [_cards addObject:card];
            }
        }
        else if ([[GameData sharedData].selectedDeckName isEqualToString:seriesSixIdentifier]) {
            for (int value = wheelygross; value <= trashosaurus; ++value) {
                Card *card = [[Card alloc] initWithSuit:suit value:value];
                [_cards addObject:card];
            }
        }
    }
}

- (id)init {
    if ((self = [super init])) {
        _cards = [NSMutableArray arrayWithCapacity:48];
        [self setUpCards];
    }
    return self;
}

- (int)cardsRemaining {
    return [_cards count];
}

- (void)shuffle {
    NSUInteger count = [_cards count];
    NSMutableArray *shuffled = [NSMutableArray arrayWithCapacity:count];
    
    for (int t = 0; t < count; ++t) {
        int i = arc4random() % [self cardsRemaining];
        Card *card = [_cards objectAtIndex:i];
        [shuffled addObject:card];
        [_cards removeObjectAtIndex:i];
    }
    
    NSAssert([self cardsRemaining] == 0, @"Original deck should now be empty");
    _cards = shuffled;
}

- (Card *)draw {
    NSAssert([self cardsRemaining] > 0, @"No more cards in the deck");
    Card *card = [_cards lastObject];
    [_cards removeLastObject];
    [_cards insertObject:card atIndex:0];
    return card;
}


@end
