//
//  AppDelegate.h
//  Splat
//
//  Created by Beau Young on 14/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
